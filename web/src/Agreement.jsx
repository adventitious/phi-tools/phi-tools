import React, {
    useState,
    useEffect,
    useCallback
} from "react";

import {
    useParams,
    useLocation,
    useHistory
} from "react-router-dom";

import {
    useQuery,
    useMutation,
    gql
} from "@apollo/client";

import {
    ChevronDownIcon,
    PlusIcon,
    DashIcon
} from "@primer/octicons-react";

import { Obligation } from "./Obligation.js";

import { SKILL_LEVELS } from "@phi/skillLevels.js";


const debug = false;

export const Agreement = ({}) => {

    const history = useHistory(); 
    const { id = "new" } = useParams();
    const location = useLocation(); 
    const queryParams = new URLSearchParams(location.search);
    const activityId = queryParams.get("activity_id") ?? undefined;

    const currentUser = {
        name: "David",
        roles: [
            "EDITOR"
        ]
    }
    
    const [ agreement, setAgreement ] = useState({
        id,
        name,
        memo: "",
        parties: [currentUser],
        activities: [],
        obligations: [null],
        criteria: []
    })

    const { loading, data } = useQuery(gql`
        query Agreement($id: ID!) {
            performanceType: performanceType(id: $id) {
                id
                name
                content
                preference
            }
            performanceTypes: performanceTypes {
                id
                name
                content
            }
        }
    `, { 
        variables: {
            id: agreement.id ?? "new"
        }
    });

    const [ createAgreement, {
        loading: agreementCreating,
        data: createdAgreement
    } ] = useMutation(gql`
        mutation Agreement(
            $id: ID!
            $title: String
            $memo: String
        ) {
            createdAgreement: createAgreement(id: $id) {
                id
                name
                memo
            }
        }
    `, { 
        variables: agreement
    });

    const [ createObligation, {
        loading: obligationCreating,
        data: createdObligation
    } ] = useMutation(gql`
        mutation Obligation(
            $agreementId: ID!
            $activityId: ID!
            $memo: String
        ) {
            createdObligation: createObligation(
                agreementId: $agreementId
                activityId: $activityId
                
            ) {
                id
                name
                memo
            }
        }
    `, { 
        variables: agreement
    });

    /*const [ createAgreement, {
        loading: agreementCreating,
        data: createdAgreement
    } ] = useMutation(gql`
        mutation Agreement(
            $id: ID!
            $title: String
            $memo: String
        ) {
            createdAgreement: createAgreement(id: $id) {
                id
                name
                memo
            }
        }
    `, { 
        variables: agreement
    });*/


    useEffect(() => {
        const resetDropdowns = () => {
        };

        document.addEventListener("click", resetDropdowns);
        return () => {
            document.removeEventListener("click", resetDropdowns);
        }
    }, [])

    const addAnObligation = useCallback(() => {
        console.log("updating the areement to add anew obligation");
        setAgreement({
            ...agreement,
             obligations: [
                ...agreement.obligations,
                null
             ]
        });
    }, [agreement]);

    if (loading) {
        return (<progress className="progress is-primary"></progress>);
    }


    return (<>

        {id === "new"
            ? 
                <section
                    className="hero is-info"
                >
                        <h1 className="title">Drafting a New Agreement</h1>
                        <p className="subtitle">
                            Consent is the foundation on which activities are performed.
                        </p>
                        <p>
                            Parties to an agreement consent to the performance of an activity, and agree that an activity should be performed in a certain way, and tht the performance should meet certain criteria. Once all parties agree to the criteria, the activities described become able to be performed.
                        </p>
                </section>
            : ""
        }

        <main>

            <h1>
                Title
                <input
                    className="input"
                    type="text"
                    placeholder="Name the Agreement..."
                />
            </h1>
            <h1>
                Description
            </h1>
            <div className="message is-info">
                <p className="message-body">
                    Enter a brief description that explains the motivations and desired outcomes of participation in the agreement.
                </p>
            </div>
            <textarea
                className="textarea"
                type="text"
                placeholder="Describe the Agreement..."
            />

            <br/>


            <div>
                <h1>Parties</h1>
                <div className="message is-info">
                    <div className="message-body">
                        <p>Entities that participate or agree to perform activities as desgribed in an agreement are considered parties to that agreement.</p>
                        <p>Each party can be assigned specific obligations which it agrees to perform once the agreement has been executed</p>
                        <p>Each party can optionally be assigned a role on the agreement which may be useful in its descriptive ability.</p>
                    </div>
                </div>
                <div className="box"
                    style={{
                        flexDirection: "column"
                    }}
                >
                    {agreement?.parties.map((party, i) => (<>
                        <div className="party"
                            style={{
                                display: "flex",
                                flexDirection: "row",
                                alignItems: "center",
                                gap: "10px"
                            }}
                        >
                            <div className="party-name">{party.name}</div>
                            <label className="party-role">
                                Role
                                <input
                                    className="input"
                                    placeholder="Role"
                                    type="text"
                                />
                            </label>
                        </div>
                    </>))}
                </div>

            </div>

            

            <div>
                <h1>Obligations</h1>
                <div className="message is-info">
                    <div className="message-body">
                        <p>Agreements create obligations to perform certain actions in a certain way.  Each obligation describes an activity that shall be performed, the capability requirements for the provider that completes the activity, the criteria by which that activity will be deemed as completed, and any other requirements as to how acceptance occurs.</p>
                        <p>Agreements need at least one obligation, and multiple obligations may be listed in a single agreement.</p>
                    </div>
                </div>
                {agreement?.obligations.map((activity, i) => (<>
                    <Obligation
                        save={() => {
                            agreement.obligations[i] = {};
                            setAgreement({
                                ...agreement,
                                obligations: [...agreement.obligations]
                            });
                        }}
                        remove={() => {
                            agreement?.obligations.splice(i, 1)
                            setAgreement({
                                ...agreement
                            });
                        }}
                        capability={() => {
                        }}
                    />
                </>))}

                
                <div
                    className="buttons is-right"
                    style={{
                        gap: "10px"
                    }}
                >
                    {agreement?.obligations.at(-1) === null
                        ? (
                            <button
                                className="button is-success is-right"
                                disabled={agreement.obligations.length <= 1}
                                onClick={() => {
                                    agreement.obligations.pop();
                                    setAgreement({ ...agreement });
                                }}
                            >
                                Done Adding Obligations
                            </button>
                        ) : (
                            <button
                                className="button is-success is-right"
                                onClick={addAnObligation}
                            >
                                Add Another Obligation
                            </button>
                        )
                    }
                </div>
            </div>

            {(debug ?? false) && (<pre>{JSON.stringify(activities, null, 2)}</pre>)}

        </main>

    </>);

}
