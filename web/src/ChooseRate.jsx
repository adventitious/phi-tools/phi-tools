import React, {
    useState
} from "react";

import { RATES } from "@phi/rates.js";

const ChooseRate = ({
    capability
}) => {

    const [ rateChooserActive, setRateChooserActive ] = useState(false);

    return (
        <div className="media">
            <div className="media-content">
                <h2>Rate</h2>
                {{
                    "STATIC": "A static rate describes a finite count of a resource, or the flow of the resource is unknown. Set the scale property to the value capably of being provided",
                    "YEARLY": "The rate at which this capability can be performed is measured on a yearly basis",
                    "QUARTERLY": "The rate at which this capability can be performed is measured on a quarterly basis",
                    "MONTHLY": "The rate at which this capability can be performed is measured on a monthly basis",
                    "WEEKLY": "The rate at which this capability can be performed is measured on a weekly basis",
                    "DAILY": "The rate at which this capability can be performed is measured on a daily basis",
                    "HOURLY": "The rate at which this capability can be performed is measured on a hourly basis"
                }[capability.rate ?? RATES[0]]}
            </div>
            <div
                className={`dropdown is-right ${rateChooserActive ? "is-active" : ""}`}
            >
                <button
                    className="dropdown-trigger button"
                    onClick={(e) => {
                        console.log("clicked dropdown")
                        setRateChooserActive(true);
                        e.preventDefault();
                        e.stopPropagation();
                        return false;
                    }}
                >
                    <span>
                        {capability.rate ?? RATES[0]}
                    </span>
                    <ChevronDownIcon className="icon"/>
                </button>
                <div
                    className="dropdown-menu is-hoverable"
                >
                    <div
                        className="dropdown-content"
                    >
                        {RATES.map((rate) => {
                            return (
                                <button
                                    className="button dropdown-item"
                                    onClick={(e) => {
                                        console.log("Updating");
                                        updateCapabilityRate(rate)
                                    }}
                                >
                                    {rate}
                                </button>
                            );
                        })}
                    </div>
                </div>
            </div>
        </div>
    );

};

