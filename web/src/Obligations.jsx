import React, {
    useState,
    useEffect
} from "react";

import {
    Link,
    useHistory
} from "react-router-dom";

import {
    useQuery,
    useMutation,
    gql
} from "@apollo/client";


export const Obligations = () => {

    const history = useHistory();

    const [ capabilities, setCapabilities ] = useState(null);
    const [ availablePreference, setAvailablePreference ] = useState(100);
    const [ allocatedPreference, setAllocatedPreference ] = useState(0);

    const { loading, data } = useQuery(gql`
        query Capabilities {
            performanceTypes: performanceTypes {
                id
                name
                content
            }
        }
    `, { });

    useEffect(() => {
        if (loading) {
            return;
        }
        setCapabilities(data.performanceTypes)
        return () => {

        }
    }, [loading]);

    if (loading || !Array.isArray(capabilities)) {
        return (<progress className="progress is-primary"></progress>);
    }

    return (<>
        <section
            className="hero is-info"
        >
            <h1 className="title">My Obligations</h1>
            <p className="subtitle">
                Trust and predictability are essential to interact and reason with others.
            </p>
            <p>
                When members of a society agree to perform some activity for the benefit of that society, society has a reasonable expectation that the performers will follow through on the performances of the activities they agreed to perform. 
                Obligations are created when all parties to an agreement consent to the performance of the activities as described in that agreement.
            </p>
            <p>
                Create obligations by becoming a party to an agreement, or draft a new agreement.
            </p>
            <div className="buttons is-right">
                <button
                    className="button is-link is-light"
                    onClick={() => {
                        history.push("/capabilities/new");
                    }}
                >
                    My Agreements
                </button>
                <button
                    className="button is-link is-light"
                    onClick={() => {
                        history.push("/capabilities/new");
                    }}
                >
                    Browse Activities
                </button>
            </div>
        </section>
        <main>
            <p>Preference: {allocatedPreference} out of {availablePreference} tokens allocated</p>
            <table className="table is-fullwidth box">

                <colgroup>
                    <col
                        span="1"
                        style={{
                            width: "10%"
                        }}
                    />
                    <col
                        span="1"
                        style={{
                            width: "20%"
                        }}
                    />
                    <col
                        span="1"
                        style={{
                            width: "70%"
                        }}
                    />
                    <col
                        span="1"
                        style={{
                            width: "10%"
                        }}
                    />
                    <col
                        span="1"
                        style={{
                            width: "10%"
                        }}
                    />
                </colgroup>

                <thead>
                    <tr>
                        {[
                            "ID",
                            "Name",
                            "Details",
                            "Appropriation",
                            "Preference"
                        ].map((title) => {
                            return (<th
                                style={{
                                    maxWidth: "100px",
                                    whiteSpace: "nowrap",
                                    overflow: "hidden",
                                    textOverflow: "ellipsis"
                                }}
                            >
                                {title}
                            </th>)
                        })}
                    </tr>
                
                </thead>

                
                {capabilities.map(({ id, name, content, preference = 0 }, index) => {
                    const goldenPreference = (tokens = 0) => {
                        return Math.pow((1 + Math.pow(5, 1/2)) / 2, tokens);
                    };
                    const gold = capabilities.reduce((acc, { preference }) => {
                        return acc + goldenPreference(preference);
                    }, 0);

                    return (
                        <tr>
                            <td
                                style={{
                                    maxWidth: "100px",
                                    whiteSpace: "nowrap",
                                    overflow: "hidden",
                                    textOverflow: "ellipsis"
                                }}
                            >
                                {id}
                            </td>
                            <td>
                                <Link
                                    to={`/capabilities/${id}`}
                                >
                                    {name}
                                </Link>
                            </td>
                            <td>{content}</td>
                            <td>
                                {Math.round(goldenPreference(preference)/gold * 10000)/100}%
                                <progress 
                                    className="progress"
                                    value={goldenPreference(preference)} max={gold}
                                />
                            </td>
                            <td>
                                <button
                                    className="button is-success is-small"
                                    disabled={allocatedPreference <= 0 || preference <= 0}
                                    onClick={() => {
                                        setAllocatedPreference(allocatedPreference - 1);
                                        setCapabilities([
                                            ...capabilities.slice(0, index),
                                            {
                                                ...capabilities[index],
                                                preference: (capabilities[index].preference ?? 1) - 1
                                            },
                                            ...capabilities.slice(index + 1, capabilities.length)
                                        ]);
                                    }}
                                >
                                    -
                                </button>
                                
                                <button 
                                    className="button is-success is-small"
                                    disabled={allocatedPreference >= availablePreference}
                                    onClick={() => {
                                        setAllocatedPreference(allocatedPreference + 1);
                                        setCapabilities([
                                            ...capabilities.slice(0, index),
                                            {
                                                ...capabilities[index],
                                                preference: (capabilities[index].preference ?? 0) + 1
                                            },
                                            ...capabilities.slice(index + 1, capabilities.length)
                                        ]);
                                    }}
                                >
                                    +
                                </button>
                                {preference}
                            </td>
                        </tr>
                    );
                })}

            </table>
        </main>
    </>);
};
