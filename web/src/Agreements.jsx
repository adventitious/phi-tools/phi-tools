import React, {
    useState,
    useEffect
} from "react";

import {
    Link,
    useHistory
} from "react-router-dom";

import {
    useQuery,
    useMutation,
    gql
} from "@apollo/client";


export const Agreements = () => {

    const history = useHistory();

    const [ capabilities, setCapabilities ] = useState(null);
    const [ availablePreference, setAvailablePreference ] = useState(100);
    const [ allocatedPreference, setAllocatedPreference ] = useState(0);

    const { loading, data } = useQuery(gql`
        query Agreements {
            performanceTypes: agreements {
                id
                name
                content
            }
        }
    `, { });

    useEffect(() => {
        if (loading) {
            return;
        }
        setCapabilities(data.performanceTypes)
        return () => {

        }
    }, [loading]);

    if (loading || !Array.isArray(capabilities)) {
        return (<progress className="progress is-primary"></progress>);
    }

    return (<>
        <section className="hero is-info">
            <h1 className="title">Agreements</h1>
            <p className="subtitle">
                Consent is the foundation on which activities are performed.
            </p>
            <p>
                Parties to an agreement consent to the performance of an activity, and agree that an activity should be performed in a certain way, and tht the performance should meet certain criteria. Once all parties agree to the criteria, the activities described become able to be performed.
            </p>
            <div className="buttons is-right">
                <button
                    className="button is-info is-medium is-light"
                    onClick={() => {
                        history.push("/agreements/new");
                    }}
                >
                    Draft a New Agreement
                </button>
            </div>
        </section>
        <main>
            <p>Preference: {allocatedPreference} out of {availablePreference} tokens allocated</p>
            <table className="table is-fullwidth box">

                <colgroup>
                    <col
                        span="1"
                        style={{
                            width: "10%"
                        }}
                    />
                    <col
                        span="1"
                        style={{
                            width: "20%"
                        }}
                    />
                    <col
                        span="1"
                        style={{
                            width: "70%"
                        }}
                    />
                    <col
                        span="1"
                        style={{
                            width: "10%"
                        }}
                    />
                    <col
                        span="1"
                        style={{
                            width: "10%"
                        }}
                    />
                    <col
                        span="1"
                        style={{
                            width: "10%"
                        }}
                    />
                </colgroup>

                <thead>
                    <tr>
                        {[
                            "ID",
                            "Name",
                            "Details",
                            "Divergence",
                            "Criticality",
                            "Actions"
                        ].map((title) => {
                            return (<th
                                style={{
                                    maxWidth: "100px",
                                    whiteSpace: "nowrap",
                                    overflow: "hidden",
                                    textOverflow: "ellipsis"
                                }}
                            >
                                {title}
                            </th>)
                        })}
                    </tr>
                
                </thead>

                
                {capabilities.map(({ id, name, content, preference = 0 }, index) => {
                    const goldenPreference = (tokens = 0) => {
                        return Math.pow((1 + Math.pow(5, 1/2)) / 2, tokens);
                    };
                    const gold = capabilities.reduce((acc, { preference }) => {
                        return acc + goldenPreference(preference);
                    }, 0);

                    return (
                        <tr>
                            <td
                                style={{
                                    maxWidth: "100px",
                                    whiteSpace: "nowrap",
                                    overflow: "hidden",
                                    textOverflow: "ellipsis"
                                }}
                            >
                                {id}
                            </td>
                            <td>
                                <Link
                                    to={`/capabilities/${id}`}
                                >
                                    {name}
                                </Link>
                            </td>
                            <td>{content}</td>
                            <td>
                                {Math.round(goldenPreference(preference)/gold * 10000)/100}%
                                <progress
                                    className="progress is-info"
                                    value={goldenPreference(preference)}
                                    max={gold}
                                />
                            </td>
                            <td>
                                {Math.round(goldenPreference(preference)/gold * 10000)/100}%
                                <progress
                                    className="progress is-danger negative"
                                    value={goldenPreference(preference)} 
                                    max={gold} 
                                />
                            </td>
                            <td>
                                <div 
                                    className="actions"
                                >
                                    <button
                                        className="button is-warning is-light"
                                        onClick={() => {
                                            history.push("/needs/new");
                                        }}
                                    >
                                        Request
                                    </button>
                                    <button
                                        className="button is-warning"
                                        onClick={() => {
                                            history.push(`/capabilities/new?activity_id=${id}`);
                                        }}
                                    >
                                        Provide
                                    </button>
                                </div>
                            </td>
                        </tr>
                    );
                })}

            </table>
        </main>
    </>);
};
