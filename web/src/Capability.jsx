import React, {
    useState,
    useEffect,
    useCallback
} from "react";

import {
    useParams,
    useLocation,
    useHistory
} from "react-router-dom";

import {
    useQuery,
    useMutation,
    gql
} from "@apollo/client";

import {
    TriangleDownIcon,
    PlusIcon,
    DashIcon
} from "@primer/octicons-react";

import { SKILL_LEVELS } from "@phi/skillLevels.js";
import { RATES } from "@phi/rates.js";

const debug = false;

export const Capability = ({}) => {

    const history = useHistory(); 
    const { id = "new" } = useParams();
    const location = useLocation(); 
    const queryParams = new URLSearchParams(location.search);
    const activityId = queryParams.get("activity_id") ?? undefined;

    const currentUser = {
        roles: [
            "EDITOR"
        ]
    }
    
    const [ activityChooserActive, setActivityChooserActive ] = useState(false);
    const [ rateChooserActive, setRateChooserActive ] = useState(false);
    const [ availablePreference, setAvailablePreference ] = useState(10);
    const [ allocatedPreference, setAllocatedPreference ] = useState(0);
    const [ activities, setActivities ] = useState([]);
    const [ activity, setActivity ] = useState({
        id: queryParams.get("activity_id") ?? undefined,
        name: undefined,
        content: undefined,
        preference: 0,
        skillRank: 0,
        scale: 0,
        rate: RATES[0]
    });
    const [ capability, setCapability ] = useState(id === "new" 
        ? {
            id,
            activityId: queryParams.get("activity_id") ?? undefined,
            name: undefined,
            content: undefined,
            preference: 0,
            skillRank: 0,
            scale: 0,
            rate: RATES[0]
        }
        : null
    );
    const [ capabilityModified, setCapabilityModified ] = useState(false);

    const { loading, data } = useQuery(gql`
        query Capabilities($id: ID!) {
            performanceType: performanceType(id: $id) {
                id
                name
                content
                preference
            }
            performanceTypes: performanceTypes {
                id
                name
                content
            }
        }
    `, { 
        variables: {
            id: activity.id ?? "new"
        }
    });

    // update/replace with new value

    const updateCapabilityProperty = (property) => (value) => {
        setCapability({
            ...capability,
            [property]: value
        })
        setCapabilityModified(true);
    };
    const updateCapabilityActivityId = useCallback((e) => {
        return updateCapabilityProperty("name")(e.target.value);
    }, [capability]);
    const updateCapabilityTitle = useCallback((e) => {
        return updateCapabilityProperty("name")(e.target.value);
    }, [capability]);
    const updateCapabilityRate = useCallback((value) => {
        return updateCapabilityProperty("rate")(value);
    }, [capability]);
    const updateCapabilityContent = useCallback((e) => {
        return updateCapabilityProperty("content")(e.target.value);
    }, [capability]);


    // +/- numeric properties

    const raiseCapabilityPreference = useCallback(() => {
        setAllocatedPreference(allocatedPreference + 1);
        setCapability({
            ...capability,
            preference: (capability.preference ?? 0) + 1
        });
        setCapabilityModified(true);
    }, [capability])
    const lowerCapabilityPreference = useCallback(() => {
        setAllocatedPreference(allocatedPreference - 1);
        setCapability({
            ...capability,
            preference: (capability.preference ?? 1) - 1
        });
        setCapabilityModified(true);
    }, [capability])
    const raiseCapabilitySkill = useCallback(() => {
        return updateCapabilityProperty("skillRank")(Math.min((capability.skillRank ?? 0) + 1, SKILL_LEVELS.length - 1))
    }, [capability])
    const lowerCapabilitySkill = useCallback(() => {
        return updateCapabilityProperty("skillRank")(Math.max(capability.skillRank - 1, 0))
    }, [capability])

    useEffect(() => {
        if (loading) {
            return;
        }
        console.log({data});
        setCapability(data?.performanceType ?? capability);
        setActivity(data?.performanceType ?? activity);
        setActivities(data?.performanceTypes)
        return () => {

        }
    }, [loading]);

    useEffect(() => {
        const resetDropdowns = () => {
            setRateChooserActive(false);
        };

        document.addEventListener("click", resetDropdowns);
        return () => {
            document.removeEventListener("click", resetDropdowns);
        }
    }, [])

    if (loading) {
        return (<progress className="progress is-primary"></progress>);
    }

    console.log({
        activityId,
        rateChooserActive

    })

    return (<>
        <div 
            className="box media"
        >
            <div 
                className="media-content"
            >

                {typeof activity.id === "undefined"
                    ? (<>
                        <h1>Select an Activity</h1>
                        <input
                            className="input"
                            placeholder="Search for an Activity"
                            onFocus={() => setActivityChooserActive(true)}
                            onBlur={() => setActivityChooserActive(false)}
                        />
                        {activities.slice(0, 5).map(activity => {
                            return (
                                <div
                                    className="media"
                                >
                                    <div
                                        className="media-content"
                                    >
                                        <h2>{activity.name}</h2>
                                        <p>{activity.content}</p>
                                    </div>
                                        <div className="buttons is-right">
                                            <button
                                                className="button is-success"
                                                onClick={() => {
                                                    setActivity(activity);
                                                }}
                                            >
                                                Select
                                            </button>
                                        </div>
                                </div>
                            );
                        })}
                        <div
                            className="media pagination is-centered"
                        >
                            <a className="pagination-previous">&lt;</a>
                            <a className="pagination-next">&gt;</a>
                            <ul className="pagination-list">
                                <li className="pagination-link">1</li>
                                <li className="pagination-link">2</li>
                                <li className="pagination-link">3</li>
                            </ul>
                            
                        </div>
                    </>)
                    : (<>
                        <div
                            className="media-content"
                        >
                            <h1>{activity.name}</h1>
                            <p>{activity.content}</p>
                        </div>
                    </>)
                }

        {typeof activity.id === "undefined"
            ? ""    
            : (<>

                        <div className="media">
                            <div className="media-content">
                                <h2>Preference</h2>
                                {capability.preference ?? 0}
                            </div>
                            <div
                                className="buttons is-right"
                            >

                                <button
                                    className="button is-success"
                                    disabled={allocatedPreference <= 0 || capability.preference <= 0}
                                    onClick={lowerCapabilityPreference}
                                >
                                    <DashIcon className="icon"/>
                                </button>
                                
                                <button 
                                    className="button is-success"
                                    disabled={allocatedPreference >= availablePreference}
                                    onClick={raiseCapabilityPreference}
                                >
                                    <PlusIcon className="icon"/>
                                </button>
                            </div>
                        </div>
                        <div className="media">
                            <div className="media-content">
                                <h2>Skill Level</h2>
                                {SKILL_LEVELS[capability.skillRank ?? 0]}
                            </div>
                            <div
                                className="buttons is-right"
                            >

                                <button
                                    className="button is-success"
                                    disabled={(capability.skillRank ?? 0) <= 0}
                                    onClick={lowerCapabilitySkill}
                                >
                                    <DashIcon className="icon"/>
                                </button>
                                
                                <button 
                                    className="button is-success"
                                    disabled={capability.skillRank >= SKILL_LEVELS.length - 1}
                                    onClick={raiseCapabilitySkill}
                                >
                                    <PlusIcon className="icon"/>
                                </button>
                            </div>
                        </div>

                        <div className="media">
                            <div className="media-content">
                                <h2>Rate</h2>
                                {{
                                    "STATIC": "A static rate describes a finite count of a resource, or the flow of the resource is unknown. Set the scale property to the value capably of being provided",
                                    "YEARLY": "The rate at which this capability can be performed is measured on a yearly basis",
                                    "QUARTERLY": "The rate at which this capability can be performed is measured on a quarterly basis",
                                    "MONTHLY": "The rate at which this capability can be performed is measured on a monthly basis",
                                    "WEEKLY": "The rate at which this capability can be performed is measured on a weekly basis",
                                    "DAILY": "The rate at which this capability can be performed is measured on a daily basis",
                                    "HOURLY": "The rate at which this capability can be performed is measured on a hourly basis"
                                }[capability.rate ?? RATES[0]]}
                            </div>
                            <div
                                className={`dropdown is-right ${rateChooserActive ? "is-active" : ""}`}
                            >
                                <button
                                    className="dropdown-trigger button"
                                    onClick={(e) => {
                                        console.log("clicked dropdown")
                                        setRateChooserActive(true);
                                        e.preventDefault();
                                        e.stopPropagation();
                                        return false;
                                    }}
                                >
                                    <span>
                                        {capability.rate ?? RATES[0]}
                                    </span>
                                    <TriangleDownIcon className="icon"/>
                                </button>
                                <div
                                    className="dropdown-menu is-hoverable"
                                >
                                    <div
                                        className="dropdown-content"
                                    >
                                        {RATES.map((rate) => {
                                            return (
                                                <button
                                                    className="button dropdown-item"
                                                    onClick={(e) => {
                                                        console.log("Updating");
                                                        updateCapabilityRate(rate)
                                                    }}
                                                >
                                                    {rate}
                                                </button>
                                            );
                                        })}
                                    </div>
                                </div>
                            </div>
                        </div>
            </>)
        }

        <div
            class="buttons is-right"
        >
            <button
                className="button is-success is-right"
                disabled={!capabilityModified}
                onClick={() => {
                    alert("saved!");
                }}
            >
                Save Changes
            </button>
        </div>
            </div>
        </div>

        {(debug ?? false) && (<pre>{JSON.stringify(activities, null, 2)}</pre>)}
    </>);

}
