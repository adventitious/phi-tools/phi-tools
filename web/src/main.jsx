
import React from "react";
import { render } from "react-dom";

import {
    BrowserRouter as Router,
} from "react-router-dom";

import {
    ApolloClient,
    InMemoryCache,
    useQuery,
    gql
} from "@apollo/client";

import { ApolloProvider } from "@apollo/client/react";

import { App } from "./App.jsx";

import "@style/phi.css";


const cache = new InMemoryCache();

const apiEndpoint =  `${window.location.origin}/graphql`;
const client = new ApolloClient({
    // Provide required constructor fields
    cache: cache,
    uri: apiEndpoint,

    // Provide some optional constructor fields
    name: "react-web-client",
    version: "1.3",
    queryDeduplication: false,
    defaultOptions: {
        watchQuery: {
            fetchPolicy: "cache-and-network",
        },
    },
});

render((
    <ApolloProvider
        client={client}
    >
        <Router>
            <App/>
        </Router>
    </ApolloProvider>
), document.getElementById("app"));

