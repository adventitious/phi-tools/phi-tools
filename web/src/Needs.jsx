import React, {
    useState,
    useEffect
} from "react";

import {
    Link,
    useHistory
} from "react-router-dom";

import {
    useQuery,
    useMutation,
    gql
} from "@apollo/client";


export const Needs = () => {

    const history = useHistory();

    const [ capabilities, setCapabilities ] = useState(null);
    const [ availablePreference, setAvailablePreference ] = useState(100);
    const [ allocatedPreference, setAllocatedPreference ] = useState(0);

    const { loading, data } = useQuery(gql`
        query Needs {
            performanceTypes: performanceTypes {
                id
                name
                content
            }
        }
    `, { });

    useEffect(() => {
        if (loading) {
            return;
        }
        setCapabilities(data.performanceTypes)
        return () => {

        }
    }, [loading]);

    if (loading || !Array.isArray(capabilities)) {
        return (<progress className="progress is-primary"></progress>);
    }

    return (<>
        <section
            className="hero is-info"
        >
            <h1 className="title">My Needs</h1>
            <div className="buttons is-right">
                <button
                    className="button is-info is-light"
                    onClick={() => {
                        history.push("/capabilities/new");
                    }}
                >
                    Add a Capability
                </button>
            </div>
        </section>
        <main>
            <p>Preference: {allocatedPreference} out of {availablePreference} tokens allocated</p>
            <table className="table is-fullwidth box">

                <colgroup>
                    <col
                        span="1"
                        style={{
                            width: "10%"
                        }}
                    />
                    <col
                        span="1"
                        style={{
                            width: "20%"
                        }}
                    />
                    <col
                        span="1"
                        style={{
                            width: "70%"
                        }}
                    />
                    <col
                        span="1"
                        style={{
                            width: "10%"
                        }}
                    />
                    <col
                        span="1"
                        style={{
                            width: "10%"
                        }}
                    />
                </colgroup>

                <thead>
                    <tr>
                        {[
                            "ID",
                            "Name",
                            "Details",
                            "Priority",
                            "Preference"
                        ].map((title) => {
                            return (<th
                                style={{
                                    maxWidth: "100px",
                                    whiteSpace: "nowrap",
                                    overflow: "hidden",
                                    textOverflow: "ellipsis"
                                }}
                            >
                                {title}
                            </th>)
                        })}
                    </tr>
                
                </thead>

                
                {capabilities.map(({ id, name, content, preference = 0 }, index) => {
                    const goldenPreference = (tokens = 0) => {
                        return Math.pow((1 + Math.pow(5, 1/2)) / 2, tokens);
                    };
                    const gold = capabilities.reduce((acc, { preference }) => {
                        return acc + goldenPreference(preference);
                    }, 0);

                    return (
                        <tr>
                            <td
                                style={{
                                    maxWidth: "100px",
                                    whiteSpace: "nowrap",
                                    overflow: "hidden",
                                    textOverflow: "ellipsis"
                                }}
                            >
                                {id}
                            </td>
                            <td>
                                <Link
                                    to={`/capabilities/${id}`}
                                >
                                    {name}
                                </Link>
                            </td>
                            <td>{content}</td>
                            <td>
                                {Math.round(goldenPreference(preference)/gold * 10000)/100}%
                                <progress 
                                    className="progress"
                                    value={goldenPreference(preference)} max={gold}
                                />
                            </td>
                            <td>
                                <button
                                    className="button is-success is-small"
                                    disabled={allocatedPreference <= 0 || preference <= 0}
                                    onClick={() => {
                                        setAllocatedPreference(allocatedPreference - 1);
                                        setCapabilities([
                                            ...capabilities.slice(0, index),
                                            {
                                                ...capabilities[index],
                                                preference: (capabilities[index].preference ?? 1) - 1
                                            },
                                            ...capabilities.slice(index + 1, capabilities.length)
                                        ]);
                                    }}
                                >
                                    -
                                </button>
                                
                                <button 
                                    className="button is-success is-small"
                                    disabled={allocatedPreference >= availablePreference}
                                    onClick={() => {
                                        setAllocatedPreference(allocatedPreference + 1);
                                        setCapabilities([
                                            ...capabilities.slice(0, index),
                                            {
                                                ...capabilities[index],
                                                preference: (capabilities[index].preference ?? 0) + 1
                                            },
                                            ...capabilities.slice(index + 1, capabilities.length)
                                        ]);
                                    }}
                                >
                                    +
                                </button>
                                {preference}
                            </td>
                        </tr>
                    );
                })}

            </table>
        </main>
    </>);
};

