import React from "react";

import { Link } from "react-router-dom";

import { Hero } from "./Hero.js";


export const Index = () => {
    return (<>
        <Hero />
        <main>
            <h1>
                Towards Decentralized Mutual Aid
            </h1>
            <p>
                Mutual aid is about making sure the people in a community have their needs met, and enabling the community to understand what work needs to be done to met those needs. Phi is an application designed to make the process of discovering what needs and capabilities exist in a community, and enabling connections to meet those needs.
            </p>
            <div
                style={{
                    display: "flex",
                    flexDirection: "row",
                    gap: "15px"
                }}
            >
                <div
                    style={{
                        background: "#f0f0f0",
                        display: "flex",
                        flexDirection: "column",
                        gap: "15px",
                        borderRadius: "5px",
                        padding: "15px"
                    }}
                >
                    <h1>
                        Declare Capabilities
                    </h1>
                    <p>
                        Mutual aid is about making sure the people in a community have their needs met, and enabling the community to understand what work needs to be done to met those needs. Phi is an application designed to make the process of discovering what needs and capabilities exist in a community, and enabling connections to meet those needs.
                    </p>
                    <Link
                        to="/capabilities"
                        className="button is-primary"
                        style={{
                            alignSelf: "center"
                        }}
                    >
                        Share a Capability
                    </Link>
                </div>
                <div
                    style={{
                        background: "#f0f0f0",
                        display: "flex",
                        flexDirection: "column",
                        gap: "15px",
                        borderRadius: "5px",
                        padding: "15px"
                    }}
                >
                    <h1>
                        Request Resources and/or Services
                    </h1>
                    <p>
                        Mutual aid is about making sure the people in a community have their needs met, and enabling the community to understand what work needs to be done to met those needs. Phi is an application designed to make the process of discovering what needs and capabilities exist in a community, and enabling connections to meet those needs.
                    </p>
                    <Link 
                        to="/needs"
                        style={{
                            alignSelf: "center"
                        }}
                        className="button is-primary">
                        Share a Need
                    </Link>
                </div>
            </div>
            <h1>
                Learn More
            </h1>
            <p>
                Mutual aid is about making sure the people in a community have their needs met, and enabling the community to understand what work needs to be done to met those needs. Phi is an application designed to make the process of discovering what needs and capabilities exist in a community, and enabling connections to meet those needs.
            </p>
        </main>
    </>);
}
