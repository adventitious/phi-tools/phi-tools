import React from "react";


export const Hero = () => {
    return (
        <section
            className="hero"
            style={{
                background: "url(\"bonnie-kittle-5US9_Gep0qY-unsplash.jpg\")",
                //background: "url(\"\")",
                background: "url(\"mathew-schwartz-ZLec57sOjlg-unsplash.jpg\")",
                background: "url(\"dixit-motiwala-DIrUrL856mQ-unsplash.jpg\")",
                backgroundSize: "cover",
                backgroundPosition: "center center",
                height: "50vh",
                alignItems: "flex-end",
                display: "flex",
            }}
        >
            <form
                style={{
                    display: "flex",
                    padding: "15px",
                    borderRadius: "5px",
                    background: "#ffffff33",
                    flexDirection: "column",
                    alignItems: "flex-end",
                    gap: "15px"
                }}
            >
                <label
                    style={{
                        color: "#fff",
                        fontWeight: "700",
                        gap: "15px",
                        display: "flex",
                        alignItems: "center"
                    }}
                >
                    Username
                    <input 
                        className="input"
                        type="text"
                    />
                </label>
                <label
                    style={{
                        color: "#fff",
                        fontWeight: "700",
                        gap: "15px",
                        display: "flex",
                        alignItems: "center"
                    }}
                >
                    Password
                    <input
                        className="input"
                        type="password"
                    />
                </label>
                <button
                    className="button is-success"
                >
                    Log In
                </button>
            </form>
        </section>
    );
};
