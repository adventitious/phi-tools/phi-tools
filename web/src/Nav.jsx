import React from "react";

import {
    Link
} from "react-router-dom";

import {
    ZapIcon,
    RocketIcon,
    GlobeIcon,
    MegaphoneIcon,
    MortarBoardIcon,
    StackIcon,
    SunIcon,
    CpuIcon,
    OrganizationIcon,
    VerifiedIcon,
    HeartIcon,
    DashIcon,
    PackageIcon,
    PackageDependenciesIcon,
    PackageDependentsIcon,
    FlameIcon,
    GraphIcon,
    ContainerIcon,
    FileIcon,
    NorthStarIcon,
    ArchiveIcon,
    BriefcaseIcon,
    CalendarIcon,
    PulseIcon,
    TelescopeIcon,
    ChecklistIcon
} from "@primer/octicons-react";

export const Nav = ({
    enabled,
    minimized,
    setMinimized
}) => {
    return (
        <nav
            className={`
                menu
                ${enabled ? "" : "hidden"}
                ${minimized ? "minimized" : ""}
            `}
            style={{
                gap: "15px"
            }} 
        >
            <button
                className="button is-small"
                style={{
                    alignSelf: "flex-end"
                }}
                onClick={() => {
                    setMinimized(!minimized)
                }}
            >
                {minimized ? ">>" : "<<"}
            </button>
            <Link
                to="/activities"
            >
                <PackageIcon className="icon"/>
                <span>
                    Activities
                </span>
            </Link>
            <Link
                to="/agreements"
            >
                <ChecklistIcon className="icon"/>
                <span>
                    Agreements
                </span>
            </Link>
            <Link
                to="/Obligations"
            >
                <GlobeIcon className="icon"/>
                <span>
                    Obligations
                </span>
            </Link>
            <Link
                to="/needs"
            >
                <PulseIcon className="icon"/>
                <span>
                    Needs
                </span>
            </Link>
            <Link
                to="/capabilities"
            >
                <MortarBoardIcon className="icon"/>
                <span>
                    Capabilities
                </span>
            </Link>
            <Link
                to="/requests"
            >
                <TelescopeIcon className="icon"/>
                <span>
                    Requests
                </span>
            </Link>
            <Link
                to="/offers"
            >
                <HeartIcon className="icon"/>
                <span>
                    Offers
                </span>
            </Link>
        </nav>
    );
};
