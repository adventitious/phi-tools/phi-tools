import gql from "graphql-tag";

export const ALL_AGREEMENTS = gql`
    query {
        agreements {
            id
            name
            content
        }
    }
`;

export const ALL_AGREEMENT_TYPES = gql`
    query {
        agreementTypes {
            id
            name
            content
        }
    }
`;

export const ALL_DOCUMENTS = gql`
    query {
        documents {
            id
            name
            content
        }
    }
`;

export const AGREEMENT_BY_ID = gql`
    query($id: ID!) {
        agreement(id: $id) {
            id
            name
            agreementType {
                id
                name
                content
            }
        }
        agreementTypes {
            id
            name
            content
        }
    }
`;

export const AGREEMENT_TYPE_BY_ID = gql`
    query($id: ID!) {
        agreementType(id: $id) {
            id
            name
            content
        }
    }
`;

export const DOCUMENT_BY_ID = gql`
    query($id: ID!) {
        document(id: $id) {
            id
            name
            content
        }
    }
`;

