import xs from "xstream";

import { 
    ALL_AGREEMENTS,
    ALL_AGREEMENT_TYPES,
    ALL_DOCUMENTS,
    AGREEMENT_BY_ID,
    AGREEMENT_TYPE_BY_ID,
    DOCUMENT_BY_ID
} from "./api/queries.js";

export const queryData = ({ router, api }) => () => {
    const routerState = router.getState();
    const pathName = routerState.name;
    try {
        return ({
            "agreements": () => api.query({
                query: ALL_AGREEMENTS,
                variables: {}
            }),
            "agreement-types": () => api.query({
                query: ALL_AGREEMENT_TYPES,
                variables: {}
            }),
            "documents": () => api.query({
                query: ALL_DOCUMENTS,
                variables: {}
            }),
            "agreements.detail": () => api.query({
                query: AGREEMENT_BY_ID,
                variables: {
                    ...routerState.params
                }
            }),
            "agreement-types.detail": () => api.query({
                query: AGREEMENT_TYPE_BY_ID,
                variables: {
                    ...routerState.params
                }
            }),
            "documents.detail": () => api.query({
                query: DOCUMENT_BY_ID,
                variables: {
                    ...routerState.params
                }
            })
        })[pathName]();
    } catch(err) {
        return xs.fromObservable({
            subscribe: () => {
                return {
                    unsubscribe: () => true
                };
            }
        });
    }
};

export const formatResponse = (response) => ({
    t: "SET_DATA",
    value: response
});

export const fetchData = ({ app, router, api }) => {
    return app.contentConnected
        .map((response) => {
            return {
                t: "SET_DATA",
                value: response
            };
        })
        .map(queryData({ router, api }))
        .flatten()
        .map(formatResponse);
};
