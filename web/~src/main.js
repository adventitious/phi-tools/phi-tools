import { run } from "@cycle/run";

import { reactor } from "./reactor.js";
import { drivers } from "./drivers.js";

run(reactor, drivers);

