import xs from "xstream";

export const intent = ({ app, router, api }) => {
    const startRouter = xs.of({ t: "START" })
        .map((e) => {
            console.log(e);
            return e;
        })
        .map(() => ({ t: "START" }));
    const connectApp = router.started
        .map((e) => {
            console.log(e);
            return e;
        })
        .filter((event) => event?.t  === "STARTED")
        .map(() => ({ t: "CONNECT" }));
    const completeTransition = router.transitions;
    const contentConnected = app.contentSet
        .map((e) => {
            console.log(e);
            return e;
        })
        .map(({ connected }) => connected)
        .flatten();
    const contentDataUpdated = app.contentSet
        .map(({ dataUpdated }) => {
            console.log(dataUpdated);
            return dataUpdated
        })
        .flatten();
    const freshen = xs.merge(api.dataRecieved);
    const load = xs.merge(contentConnected);
    const reconcile = xs.merge(contentDataUpdated);
    const hideSidebar = xs.never();
    const showSidebar = xs.never();
    return {
        startRouter,
        hideSidebar: xs.never(),
        showSidebar: xs.never(),
        connectApp,
        load,
        freshen,
        hideSidebar,
        showSidebar,
        reconcile,
        subscribe: xs.never(),
        completeTransition,
        changeRoute: xs.never()
    };
};
