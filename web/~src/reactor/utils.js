import { constantify } from "inflected";
import xs from "xstream";


export const set = (streamsToSet) => Object
    .entries(streamsToSet)
    .reduce((settingStream, [key, stream]) => ({
        ...settingStream,
        [camelize(`set-${key}`)]: stream.map((value) => ({t: constantify(`SET_${key}`), value}))
    }), {});

export const transition = (streamsToSet) => Object
    .entries(streamsToSet)
    .reduce((settingStream, [key, stream]) => ({
        ...settingStream,
        [key]: stream.map((value) => ({t: constantify(`TRANSITION_${key}`), value}))
    }), {});

export const operation = (streamsToSet) => Object
    .entries(streamsToSet)
    .reduce((settingStream, [key, stream]) => ({
        ...settingStream,
        [key]: stream.map((value) => ({t: constantify(`OPERATION_${key}`), value}))
    }), {});


