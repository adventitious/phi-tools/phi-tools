import xs from "xstream";
import dropRepeats from "xstream/extra/dropRepeats";

export const model = ({
    hideSidebar,
    showSidebar,
    load,
    reconcile,
    changeRoute,
    connectApp,
    completeTransition,
    subscribe,
    startRouter
}) => {
    const sidebarVisibility = xs
        .merge(
            hideSidebar.mapTo(false),
            showSidebar.mapTo(true)
        )
        .compose(dropRepeats());
    const query = load;
    const mutation = reconcile;
    const startingRouter = startRouter;
    const subscription = subscribe;
    const connectingApp = connectApp;
    const pendingRoute = xs.merge(
        changeRoute.map(({ targetRoute }) => targetRoute),
        completeTransition.map(() => null)
    );
    const currentRoute = completeTransition.map(({ route }) => route);
    const previousRoute = completeTransition.map(({ previousRoute }) => previousRoute);
    const targetRoute = xs.never();
    const content = xs.never();
    return {
        sidebarVisibility,
        pendingRoute,
        currentRoute,
        previousRoute,
        query,
        mutation,
        subscription,
        targetRoute,
        startingRouter,
        connectingApp
    };
};

