import xs from "xstream";
import { router } from "./router.js";

/*
const validateAndRemapSinkArgument = (arg) => {
    if (!arg || !arg.length) {
        return null;
    };
    if (typeof arg === 'string') {
        arg = [arg];
    }
    else if (!(arg instanceof Array)) {
        throw new Error('A Router5 sink argument should be a string or an array of arguments, starting with a function name');
    }
    if (validSinkFuncs.indexOf(arg[0]) === -1) {
        throw new Error(`"${arg[0]}" is not the name of a valid sink function call for the Router5 driver`);
    }
    if (typeof arg[arg.length - 1] === 'function') {
        throw new Error('Router5 invocations specifying callbacks should be made using the source (responses) object');
    }
    return arg;
}

cosnt createStateChange = (router, fname, args) => {
    return {
        subscribe: (observer) => {
            try {
                router[fname].apply(router, args.concat((toState, fromState) => {
                    observer.next({ toState, fromState });
                }));
            } catch(e) {
                observer.error(e);
            }
            
        }
    };
}

const createDone(router, method, args) => {
    return {
        subscribe: (observer) => {
            try {
                router[method].apply(router, args.concat(() => {
                    observer.next(true);
                    observer.complete();
                }));
            } catch(e) {
                observer.error(e);
            }
        }
    };
}
*/

export const make = () => {
    return (sunk) => {
        const routerTarget = new EventTarget();
        sunk.subscribe({
            next: ({ t, value }) => {
                console.log("got", t, value);
                ({
                    OPERATION_START_ROUTER: () => {
                        router.start();
                        routerTarget.dispatchEvent(new Event("STARTED"));
                    }
                })[t](value);
            }
        });
        const started = xs.fromObservable({
            subscribe: (listener) => {
                routerTarget.addEventListener("STARTED", () => {
                    console.log("Started...")
                    listener.next({
                        t: "STARTED"
                    });
                });  
            },
            unsubscribe: () => {
                return;
            }
        }).startWith(null);
        const transitions = xs.fromObservable(router).startWith({
            route: router.getState(),
            previousRoute: null
        });
        return {
            started,
            getState: () => router.getState(),
            transitions
        };
    };
};
