export const routes = [
    {
        name: "landing",
        path: "/"
    },
    {
        name: "obligations",
        path: "/obligations",
        children: [{
            name: "detail",
            path: "/:id"
        }]
    },
    {
        name: "collections",
        path: "/collections",
        children: [{
            name: "detail",
            path: "/:id"
        }]
    },
    {
        name: "agreements",
        path: "/agreements",
        children: [{
            name: "detail",
            path: "/:id"
        }]
    },
    {
        name: "agreement-types",
        path: "/agreement-types",
        children: [{
            name: "detail",
            path: "/:id"
        }]
    },
    {
        name: "performances",
        path: "/performances",
        children: [{
            name: "detail",
            path: "/:id"
        }]
    },
    {
        name: "documents",
        path: "/documents",
        children: [{
            name: "detail",
            path: "/:id"
        }]
    }
];

