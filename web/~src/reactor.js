import { pipe } from "@phi/utils/pipe.js";

import { intent } from "./reactor/intent.js";
import { model } from "./reactor/model.js";
import { effect } from "./reactor/effect.js";
import { drain } from "./reactor/drain.js";

export const reactor = (sources) => pipe(sources)([
    intent,
    model,
    effect,
    drain
]);
