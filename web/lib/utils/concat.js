export const concat = (x, xs) => {
    if (Array.isArray(xs)) {
        return [...xs, x];
    }
    return [x];
};

