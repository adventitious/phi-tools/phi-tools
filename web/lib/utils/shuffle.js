export const shuffle = (array) => {
    return array.map((element) => {
        return [Math.random(), element];
    }).sort(([r1], [r2]) => {
        return r1 - r2;
    }).map(([, e]) => {
        return e;
    });
};
