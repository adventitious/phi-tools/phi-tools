import Inflector from "inflected";

const { pluralize } = Inflector;

export const flatDecollate = (collated) => {
    return collated.reduce((partiallyDecollated, objectToDecollate) => {
        return Object.entries(objectToDecollate)
            .reduce((partiallyDecollated, [key, value]) => {
                const pluralKey = pluralize(key);
                const partiallyDecollatedValueForKey = Array.isArray(partiallyDecollated[pluralKey])
                    ? partiallyDecollated[pluralKey] : [];
                const flattableValue = Array.isArray(value) ? value : [value];
                return {
                    ...partiallyDecollated,
                    [pluralize(key)]: [...partiallyDecollatedValueForKey, ...flattableValue]
                };
            }, partiallyDecollated);
    }, {});
};

