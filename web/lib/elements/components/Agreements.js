import xs from "xstream";

import { create as Table } from "./phi-table.js";

customElements.define(
    "phi-agreements",
    class extends HTMLElement {
        constructor() {
            super();
            this.hideSidebar = false;
            this.connected = xs.fromObservable({
                subscribe: (listener) => {
                    this.addEventListener("CONNECTED", () => {
                        console.log("GOT CONNECTED");
                        listener.next({t: "CONNECTED"});
                    });
                },
                unsubscribe: () => {}
            });
            this._heading = document.createElement("h1");
            this._heading.innerHTML = "Agreements";
            this._listing = Table({
                columns: [{
                    heading: "ID",
                    formatRecord: (record) => {
                        const element = document.createElement("a");
                        element.href = `/agreements/${record.id}`;
                        element.innerHTML = record.id;
                        return element;
                    }
                }, {
                    heading: "Name",
                    formatRecord: (record) => {
                        const element = document.createElement("a");
                        element.href = `/agreements/${record.id}`;
                        element.innerHTML = record.name;
                        return element;
                    }
                }]
            });
            this._actions = document.createElement("div");
            this._actions.classList.add("actions");
            this._new = document.createElement("a");
            this._new.classList.add("button");
            this._new.innerHTML = "New Agreement";
            this._new.href = "/agreements/new";
            this._actions.replaceChildren(this._new);
            console.log();
        }
        connectedCallback() {
            console.log("WILL SEND CONNECTED");
            this.append(this._heading, this._listing, this._actions);
            this.dispatchEvent(new Event("CONNECTED"));
        }
        setData({ data }) {
            this._listing.setData(data.agreements);
        }
    }
);

export const create = () => {
    const element = document.createElement("phi-agreements");
    return element;
};

