import fromEvent from "xstream/extra/fromEvent";

import { defineElement } from "@phi/elements/utils/defineElement.js";

import { TextArea } from "../elements/MarkdownRenderer.js";

import Loading from "./Loading.js";
import MarkdownRenderer from "./MarkdownRenderer.js";
import Component from "./Component.js";

export default defineElement(class EditableContent extends Component {
    static create({ value }) {
        const el = super.create();
        el.streams.value = value;
        return el;
    }
    constructor() {
        super();
        this.elements = {
            textArea: TextArea.create({
                placeholder: "Enter document content here. Markdown syntax is supported",
                classes: ["textarea"]
            }),
            rendered: MarkdownRenderer.create({ 
                value: this.streams.value
            })
        };
        this.streams = {
            connected: fromEvent(this, "CONNECTED") 
        };
    } 
    connectedCallback() {
        super.connectedCallback();
        this.textArea.addEventListener("input", () => {
            this.streams.value = this.textArea.value;
        });
        this.elements.rendered.streams.value = this.elements.textArea.streams.value;
        if (this._loading) {
            this._content = Loading.create();
            this.replaceChildren(this._content);
            this.dispatchEvent(new Event("CONNECTED"));
        } else {
            this.elements.textArea.value = this._value;
            this.replaceChildren(this._content, this.textArea);
            this._save.addEventListener("click", () => {
                this.dispatchEvent(new Event("SAVE"));
            });
            this.dispatchEvent(new Event("CONNECTED"));
        }
    }
});

