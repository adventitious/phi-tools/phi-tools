import { defineElement } from "@phi/elements/utils/defineElement.js";
import Component from "./Component.js";

export default defineElement(class Content extends Component {
    connectedCallback() {
        this.innerHTML = "Accountability Matters.";
    }
});
