import marked from "marked";
import dropRepeats from "xstream/extra/dropRepeats";
import fromEvent from "xstream/extra/fromEvent";

import { defineElement } from "@phi/elements/utils/defineElement.js";

import Div from "../DOM/Div.js";
import H1 from "../DON/H1.js";
import EditableContent from "./EditableContent.js";
import Loading from "./Loading.js";
import NotFound from "./NotFound.js";
import Component from "./Component.js";
import Button from "./Button.js";
import Select from "./Select.js";

export default defineElement(class NodeDetail extends Component {
    static create({ doc }) {
        const el = super.create();
        if (typeof doc !== "undefined") {
            el.setData(doc);
        }
        return el;
    }
    constructor() {
        super();
        this.hideSidebar = false;
        this.streams = {
            connected: () => fromEvent(this, "CONNECTED").mapTo({t: "CONNECTED"}),
            dataUpdated: () => fromEvent(this, "SAVE").map(() => this._data),
            documentContent: () => this.streams.document
                .map(doc => doc.content)
                .compose(dropRepeats)
        };
        this._heading = H1.create({});
        this._actions = Div.create({
            classList: ["actions"],
            children: Button.create({
                innerHTML: "Save"
            })
        });
        this._selectAgreementType = Select.create({});
        this._content = Loading();
    }
    get _data() {
        return this.nodes;
    }
    connectedCallback() {
        this._contentEditorTA.addEventListener("input", () => {
            this._contentMd = this._contentEditorTA.value;
            this._content.innerHTML = marked(this._contentMd);
        });
        if (this._loading) {
            this._content = Loading();
            this._contentWrapper.replaceChildren(this._content);
            this.replaceChildren(this._contentWrapper);
            this.dispatchEvent(new Event("CONNECTED"));
        } else {
            this._content = EditableContent.create({ value: this.streams.documentContent });
            this._contentWrapper.replaceChildren(this._content, this._contentEditorTA);
            this._content.innerHTML = marked(this._contentMd);
            this._save.addEventListener("click", () => {
                this.dispatchEvent(new Event("SAVE"));
            });
            this.replaceChildren(this._heading, this._contentWrapper, this._actions);
            this.dispatchEvent(new Event("CONNECTED"));
        }
    }
    get _contentMd() {
        if (this._data.document === null) {
            return "";
        }
        if (typeof this._data.document.content === "string") {
            return this._data.document.content;
        }
        return "";
    }
    set _content(content) {
        if (this._data.document === null) {
            this._data.document = {};
        }
        if (typeof content === "string") {
            this._data.document.content = content;
        } else {
            this._data.document.content = "";
        }
    }
    setData({ data }) {
        try {
            this._data = data;
            if (this._data.agreementType === null) {
                this._data.agreementType = {};
            }
            this._contentEditorTA.value = this._contentMd;
            this._content = document.createElement("div");
            this._content.classList.add("content-preview");
            this._contentWrapper.replaceChildren(this._content, this._contentEditorTA);
            this._heading.innerHTML = "New Agreement Type";
            if (this._loading) {
                this._loading = false;
                this.replaceChildren(this._heading, this._contentWrapper, this.actions);
                this._save.addEventListener("click", () => {
                    this.dispatchEvent(new Event("SAVE"));
                });
            }
            this._heading.innerHTML = data.agreementType.name;
            this._contentWrapper.innerHTML = marked(data.agreementType.content);
        } catch(err) {
            console.error(err);
            this.replaceChildren(NotFound());
            if (this._loading) {
                this._loading = false;
            }
        }
    }
    setAgreementType(agreementTypeId) {
        this._agreementType = this._agreementTypes.find(({id}) => id === agreementTypeId);
    }
});
