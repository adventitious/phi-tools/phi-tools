import Component from "./Component.js";
import { defineElement } from "../utils/defineElement.js";

export default defineElement(
    class NavItem extends Component {
        static create({
            href,
            text,
            classNames = []
        }) {
            const element = document.createElement("a");
            const textNode = document.createTextNode(text);
            element.setAttribute("href", href);
            element.classList.add(...classNames, "navbar-item");
            element.append(textNode);
            return element;
        };

        constructor() {
            super();
        }
    }
);
