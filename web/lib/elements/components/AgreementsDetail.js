import xs from "xstream";
import marked from "marked";

import { create as Loading } from "./phi-loading";
import { create as NotFound } from "./phi-not-found.js";
import { create as Select } from "./phi-select.js";

customElements.define(
    "phi-agreements-detail",
    class extends HTMLElement {
        constructor() {
            super();
            this.hideSidebar = false;
            this.connected = xs.fromObservable({
                subscribe: (listener) => {
                    this.addEventListener("CONNECTED", () => {
                        listener.next({t: "CONNECTED"});
                    });
                },
                unsubscribe: () => {}
            });
            this._heading = document.createElement("h1");
            this._contentWrapper = document.createElement("div");
            this._contentWrapper.classList.add("document-content");
            this._actions = document.createElement("div");
            this._actions.classList.add("actions");
            this._save = document.createElement("a");
            this._save.innerHTML = "Save";
            this._selectAgreementType = Select({});
            this._actions.replaceChildren(this._save);
            this._content = Loading();
            console.log();
        }
        connectedCallback() {
            console.log("WILL SEND CONNECTED");
            this.replaceChildren(this._heading, this._selectAgreementType, this._contentWrapper, this._actions);
            this.dispatchEvent(new Event("CONNECTED"));
        }
        setData({ data }) {
            try {
                if (data.agreement.agreementType === null) {
                    this._heading.innerHTML = "Select Agreement Type";
                }
                this._agreementTypes = data.agreementTypes;
                this._selectAgreementType.options = this._agreementTypes
                    .map(({ id, name }) => ({ value: id, name: name }));
                this._selectAgreementType.addEventListener("change", () => {
                    this.setAgreementType(this._selectAgreementType.value);
                });
            } catch(err) {
                console.error(err);
                this._heading.innerHTML = "";
                this._contentWrapper.replaceChildren(NotFound());
            }
        }
        setAgreementType(agreementTypeId) {
            this._agreementType = this._agreementTypes.find(agreementTypeId);
            this._contentWrapper.innerHTML = marked(this._agreementType.content);
        }
    }
);

export const create = ({
    doc
}) => {
    const element = document.createElement("phi-agreements-detail");
    if (typeof doc !== "undefined") {
        element.setData(doc);
    }
    return element;
};

