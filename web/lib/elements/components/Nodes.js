import fromEvent from "xstream/extra/fromEvent";

import Table from "./Table.js";
import {
    Link,
    H1,
    Div
} from "../DOM.js";

export class NodeList extends HTMLElement {
    constructor() {
        super();
        this.hideSidebar = false;
        this.streams = {
            connected: fromEvent(this, "CONNECTED")
                .mapTo({t: "CONNECTED"}),
            dataUpdated: fromEvent(this, "SAVE")
                .map(() => this.node)
        };
        this._heading = H1.create("Agreemet Types");
        this._listing = Table.create({
            columns: [{
                heading: "ID",
                formatRecord: (record) => Link.create({
                    href: `/agreement-types/${record.id}`,
                    display: record.name
                })
            }, {
                heading: "Name",
                formatRecord: (record) => Link.create({
                    href: `/agreement-types/${record.id}`,
                    display: record.name
                })
            }]
        });
        this._actions = Div.create({
            classList: ["actions"],
            children: [
                Link.create({
                    classList: ["button"],
                    display: "New Agreement Type",
                    href: "/agreement-types/new"
                })
            ]
        });
        console.log();
    }
    connectedCallback() {
        console.log("WILL SEND CONNECTED");
        this.append(this._heading, this._listing, this._actions);
        this.dispatchEvent(new Event("CONNECTED"));
    }
    setData({ data }) {
        this._listing.setData(data.agreementTypes);
    }
}
customElements.define("phi-nodes", NodeList);

export const create = () => {
    const element = document.createElement("phi-agreement-types");
    return element;
};


