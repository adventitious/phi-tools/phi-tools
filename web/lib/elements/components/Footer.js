import Component from "./Component.js";
import { defineElement } from "../utils/defineElement.js";

export default defineElement(
    class Footer extends Component {
        static create() {
            const element = document.createElement(this.elementName);
            element.classList.add("footer");
            return element;
        };
        constructor() {
            super();
        }
        connectedCallback() {
            this.innerHTML = `
                &phi;
            `;
        }
    }
    constructor() {
        super();
    }
    connectedCallback() {
        this.innerHTML = `
            &phi;
        `;
    }
);

