
customElements.define(
    "phi-select",
    class extends HTMLElement {
        constructor() {
            super();
            this._options = [];
            this._select = document.createElement("select");
        }
        connectedCallback() {
            this.classList.add("select");
            this._select.addEventListener("change", () => {
                this.setAgreementType(this._select.value);
            });
            this.replaceChildren(this._select);
        }
        set options(options) {
            this._options = options;
            console.log(options);
            this._select.replaceChildren(...options.map(({ name, value }) => {
                const option = document.createElement("option");
                option.setAttribute("value", value);
                option.innerHTML = name;
                return option;
            }));
        }
        get options() {
            return this._options;
        }
    }
);

export const create = ({ options } = {}) => {
    const element = document.createElement("phi-select");
    if (Array.isArray(options)) {
        element.options = options;
    }
    return element;
};
