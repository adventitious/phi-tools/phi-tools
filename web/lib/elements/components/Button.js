import Component from "./Component";

export default class Button extends Component {
    static create({
        display,
        onclick
    }) {
        const element = super.create();
        element.innerHTML = display;
        element.addEventListener("click", onclick);
        return element;
    }
}

