import fromEvent from "xstream/extra/fromEvent";
import marked from "marked";

import { defineElement } from "@phi/element/utils/defineElement.js";

import Component from "./Component.js";
import MarkdownRenderer from "./MarkdownRenderer.js";
import NotFound from "./NotFound.js";

export default defineElement(class AgreementTypeDetail extends Component {
    static create({ doc }) {
        const element = document.createElement("phi-agreement-type-detail");
        if (typeof doc !== "undefined") {
            element.setData(doc);
        }
        return element;
    }
    constructor() {
        super();
        this.hideSidebar = false;
        this.connected = fromEvent(this, "CONNECTED");
        this._heading = document.createElement("h1");
        this._actions = document.createElement("div");
        this._actions.classList.add("actions");
        this._save = document.createElement("a");
        this._save.innerHTML = "Save";
        this._selectAgreementType = document.createElement("select");
        this._actions.replaceChildren(this._save);
    }
    connectedCallback() {
        if (this._loading) {
            this._content = MarkdownRenderer();
            this.replaceChildren(this._content);
            this.dispatchEvent(new Event("CONNECTED"));
        } else {
            this._content = document.createElement("div");
            this._contentEditorTA.value = this._contentMd;
            this._contentWrapper.replaceChildren(this._content, this._contentEditorTA);
            this._content.innerHTML = marked(this._contentMd);
            this._save.addEventListener("click", () => {
                this.dispatchEvent(new Event("SAVE"));
            });
            this.replaceChildren(this._heading, this._contentWrapper, this._actions);
            this.dispatchEvent(new Event("CONNECTED"));
        }
    }
    setData({ data }) {
        try {
            this._data = data;
            if (this._data.agreementType === null) {
                this._data.agreementType = {};
            }
            this._contentEditorTA.value = this._contentMd;
            this._content = document.createElement("div");
            this._content.classList.add("content-preview");
            this._contentWrapper.replaceChildren(this._content, this._contentEditorTA);
            this._heading.innerHTML = "New Agreement Type";
            if (this._loading) {
                this._loading = false;
                this.replaceChildren(this._heading, this._contentWrapper, this.actions);
                this._save.addEventListener("click", () => {
                    this.dispatchEvent(new Event("SAVE"));
                });
            }
            this._heading.innerHTML = data.agreementType.name;
            this._contentWrapper.innerHTML = marked(data.agreementType.content);
        } catch(err) {
            console.error(err);
            this.replaceChildren(NotFound());
            if (this._loading) {
                this._loading = false;
            }
        }
    }
    setAgreementType(agreementTypeId) {
        this._agreementType = this._agreementTypes.find(({id}) => id === agreementTypeId);
    }
    get _contentMd() {
        if (this._data.document === null) {
            return "";
        }
        if (typeof this._data.document.content === "string") {
            return this._data.document.content;
        }
        return "";
    }
    set _content(content) {
        if (this._data.document === null) {
            this._data.document = {};
        }
        if (typeof content === "string") {
            this._data.document.content = content;
        } else {
            this._data.document.content = "";
        }
    }
});

