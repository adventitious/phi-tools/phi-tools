import { defineElement } from "../utils/defineElement.js";
import Component from "./Component.js";


export default defineElement(
    class Nav extends Component {
        static create({
            brand,
            start,
            end
        }) {
            const element = document.createElement(this.elementName);
            element.setNavItems({ brand, start, end });
            return element;
        };

        constructor() {
            super();
            this._isActive = false;
            this._brand = document.createElement("div");
            this._brand.classList.add("navbar-brand");
            this._burger = document.createElement("a");
            this._burger.classList.add("navbar-burger");
            this._burger.innerHTML = `
                <span aria-hidden="true"></span>
                <span aria-hidden="true"></span>
                <span aria-hidden="true"></span>
            `;
            this._burger.addEventListener("click", () => {
                if (this._isActive === true) {
                    this._menu.classList.add("is-active");
                    this._burger.classList.add("is-active");
                    this._isActive = false;
                } else {
                    this._menu.classList.remove("is-active");
                    this._burger.classList.remove("is-active");
                    this._isActive = true;
                }
            });
            this._menu = document.createElement("div");
            this._menu.classList.add("navbar-menu");
            this._start = document.createElement("div");
            this._start.classList.add("navbar-start");
            this._end = document.createElement("div");
            this._end.classList.add("end");
        }
        setNavItems({ brand, start, end }) {
            this._navItems = { brand, start, end };
            this._brand.append(this._navItems.brand, this._burger);
            this._start.append(...this._navItems.start);
            this._end.append(...this._navItems.end);
            this._menu.replaceChildren(this._start, this._end);
        }
        connectedCallback() {
            this.replaceChildren(
                this._brand,
                this._menu,
            );
            this.classList.add("navbar");
        }
    }
);

