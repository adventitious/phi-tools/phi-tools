import fromEvent from "xstream/extra/fromEvent";

import { defineElement } from "@phi/elements/utils/defineElement.js";
import Component from "./Component.js";

export default defineElement(class Landing extends Component {
    constructor() {
        super();
        this.hideSidebar = true;
        this.connected = fromEvent(this, "CONNECTED");
    }
    connectedCallback() {
        this.innerHTML = `
            <div class="hero">
                <h1
                >
                    Accountability Matters.
                </h1>
                <p
                >
                    <a
                        href="https://unsplash.com/photos/6iM5GOht664"
                    >
                        Photo
                    </a>
                    &nbsp;by&nbsp;
                    <a
                        href="https://unsplash.com/@karim_manjra?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText"
                    >
                        Karim MANJRA
                    </a>
                    &nbsp;on&nbsp;
                    <a
                        href="https://unsplash.com/s/photos/freedom?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText"
                    >
                        Unsplash
                    </a>
                </p>
            </div>
            <div class="how">
                <h1
                    style={{
                        fontSize: "32px"
                    }}
                >
                    Trust But Verify
                </h1>
                <ul
                    style={{
                        fontSize: "17px"
                    }}
                >
                    
                    <li>Establish Responsibility to Fulfill Obligations</li>
                    <li>Structure Incentives to Foster Initiative</li>
                    <li>Communicate Pressure to Elevate Accountability</li>
                    <li>Document, Review, and Audit Obligation Performances</li>
                </ul>
            </div>
            <div class="what"
                style={{
                }}

            >
                <h1
                    style={{
                        fontSize: "32px"
                    }}
                >
                    A Framework for Managing Obligations
                </h1>
                <ul
                    style={{
                        fontSize: "17px"
                    }}
                >
                    <li>
                        Performance types abstractly describe an action, deliverable, premise, or thought with be performed or progressed.
                    </li>
                    <li>
                        Obligations create responsibility to execute some type of performance.
                    </li>
                    <li>
                        Collections of performance types enable the organization and management of obligations.
                        <Link>Collections</Link>
                    </li>
                    <li>
                        Execute agreements to fulfill obligations or collections of obliations in order to document or demonstrate how performances fulfill those obligations/ 
                    </li>
                    <li>
                        &Phi;.tools provide a framework for creating accountability.
                    </li>
                </ul>
            </div>
        `;
        this.dispatchEvent(new Event("CONNECTED"));
    }
});

