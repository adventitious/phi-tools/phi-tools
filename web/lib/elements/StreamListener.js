import xs, { Stream } from "xstream";

import { coalesce } from "../../utils/coalesce.js";


export const addListener = Stream.prototype.addListener.apply.bind(null);
export const removeListener = Stream.prototype.removeListener.apply.bind(null);
const mapListenersToStreams = (listeners, streams) => coalesce(streams, listeners)
    .map(addListener);
const unmapListenersFromStreams = (listeners, streams) => coalesce(streams, listeners)
    .map(removeListener);

export default class StreamListener extends HTMLElement {
    constructor() {
        super();
        this._streams = {};
        this._listeners = {};
        this._streamsProxy = new Proxy(this._streams, {
            get: (obj, prop) => {
                if (prop in obj) {
                    return obj[prop];
                }
                return xs.never();
            },
            set: (obj, prop, value) => {
                if (prop in obj) {
                    obj[prop].removeListener(this.listeners[prop]);
                }
                obj[prop] = value;
                obj[prop].addListener(this.listeners[prop]);
            } 
        });
        this._listenersProxy = new Proxy(this._listeners, {
            get: (obj, prop) => {
                if (prop in obj) {
                    return obj[prop];
                }
                return xs.never();
            },
            set: (obj, prop, value) => {
                if (prop in obj) {
                    obj[prop].removeListener(this.listeners[prop]);
                }
                obj[prop] = value;
                obj[prop].addListener(this.listeners[prop]);
            } 
        });
    }
    connectedCallback() {
        super.connectedCallback();
        console.log("mapping");
        if (this.isConnected) {
            mapListenersToStreams(this.listeners, this.streams);
        }
    }
    disconnectedCallback() {
        console.log("unmapping");
        console.log(this.listeners, this.streams)
        unmapListenersFromStreams(this.listeners, this.streams);
    }

    // Provide a set of streams to the Listener, replacing any existing streams.
    // Places the streams into the same object, so references are kept fresh.
    set streams(value) {
        // Remove the existing listeners.
        unmapListenersFromStreams(this.listeners, this.streams);
        // Remove the existing streams.
        Object.keys(this._streams)
            .forEach((name) => {
                delete this._streams[name];
            });
        // Add the new streams.
        Object.entries(value)
            .forEach(([name, stream]) => {
                this._streams[name] = stream;
            });
        // If the component is connected set up the listeners again.
        if (this.isConnected) {
            mapListenersToStreams(this.listeners, this.streams);
        }
    }

    // Get the listeners that are currently defined.
    get listeners() {
        return this._listeners;
    }

    // Get the streams that are provided to the Listener.
    get streams() {
        // Supporting default values for a stream and dynamically setting them
        // by returning a proxy of the streams dict to lookup a
        return this._streamsProxy;
    }

}
