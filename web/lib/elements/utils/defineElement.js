export const defineElement = (El) => {
    customElements.define(El.elementName, El);
    return El;
};
