resource "aws_security_group" "lb" {
    name                 = "${var.application.organization.name}-${var.application.name}-${var.network.name}-lb-sg"
    vpc_id               = var.network.vpc.id
    ingress {
        protocol         = "tcp"
        from_port        = 80
        to_port          = 80
        cidr_blocks      = ["0.0.0.0/0"]
    }
    ingress {
        protocol         = "tcp"
        from_port        = 443
        to_port          = 443
        cidr_blocks      = ["0.0.0.0/0"]
    }
    egress {
        protocol         = "-1"
        from_port        = 0
        to_port          = 0
        cidr_blocks      = ["0.0.0.0/0"]
        ipv6_cidr_blocks = ["::/0"]
    }
    tags = {
        Name            = "${var.application.name} ${var.network.name} LB Security Group"
        ApplicationName = var.application.name
        BusinessOwner   = var.application.business_owner
        Division        = var.application.division.name
        Environment     = var.network.name
        OperationOwner  = var.application.operation_owner
    }
}

resource "aws_lb_target_group" "http" {
    name        = "${var.application.organization.name}-${var.application.name}-nginx-http-tg-${var.network.name}"
    port        = 80
    protocol    = "TCP"
    vpc_id      = var.network.vpc.id
    target_type = "ip"
    health_check {
        healthy_threshold   = "3"
        unhealthy_threshold = "3"
        interval            = "30"
        protocol            = "TCP"
    }
    stickiness {
        enabled = false
        type = "lb_cookie"
    }
    tags = {
        Name            = "${var.application.name} ${var.network.name} LB Redirect Target Group"
        ApplicationName = var.application.name
        BusinessOwner   = var.application.business_owner
        Division        = var.application.division.name
        Environment     = var.network.name
        OperationOwner  = var.application.operation_owner
    }
}

resource "aws_lb_target_group" "https" {
    name        = "${var.application.organization.name}-${var.application.name}-nginx-https-tg-${var.network.name}"
    port        = 443
    protocol    = "TCP"
    vpc_id      = var.network.vpc.id
    target_type = "ip"
    health_check {
        healthy_threshold   = "3"
        unhealthy_threshold = "3"
        interval            = "30"
        protocol            = "TCP"
    }
    stickiness {
        enabled = false
        type = "lb_cookie"
    }
    tags = {
        Name            = "${var.application.name} ${var.network.name} LB Encrypted Target Group"
        ApplicationName = var.application.name
        BusinessOwner   = var.application.business_owner
        Division        = var.application.division.name
        Environment     = var.network.name
        OperationOwner  = var.application.operation_owner
    }
}

resource "aws_lb_listener" "http" {
    load_balancer_arn = var.network.load_balancer.id
    port              = 80
    protocol          = "TCP"
    default_action {
        target_group_arn = aws_lb_target_group.http.id
        type             = "forward"
    }
    #default_action {
    #    type = "redirect"
    #
    #    redirect {
    #        port        = 443
    #        protocol    = "HTTPS"
    #        status_code = "HTTP_301"
    #    }
    #}
}
 
resource "aws_lb_listener" "https" {
    load_balancer_arn = var.network.load_balancer.id
    port              = 443
    protocol          = "TCP"
    #ssl_policy        = "ELBSecurityPolicy-2016-08"
    #certificate_arn = aws_acm_certificate.https_certificate.arn
    #certificate_arn = "arn:aws:acm:us-east-1:484737932465:certificate/1b45f7a9-528e-4c80-91f6-91fe654d809a"
    
    default_action {
        target_group_arn = aws_lb_target_group.https.id
        type             = "forward"
    }
}
