variable "branch" {
    description = "The branch to use for the environment"
    type = string
    default = "master"
}

variable "application" {
    description = "Application object"
    type = object({
        roles = object({
            task = object({
                id = string
                arn = string
            })
            task_execution = object({
                id = string
                arn = string
            })
        })
        name = string
        organization = object({
            name = string
        })
        business_owner = string
        division = object({
            name = string
        })
        operation_owner = string
    })
}

variable "network" {
    description = "the network to put the gateway in"
    type = object({
        name = string
        vpc = object({
            id = string
        })
        subnets = object({
            public = list(object({
                id = string
            }))
            private = list(object({
                id = string
            }))
        })
        dns_namespace = object({
            id = string
        })
        load_balancer = object({
            id = string
        })
    })
}

variable "cluster" {
    description = "The name of the environment"
    type = object({
        cluster = object({
            name = string
            id = string
        })
    })
}
