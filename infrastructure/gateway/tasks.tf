resource "aws_ecs_task_definition" "gateway" {
    family                   = "${var.application.organization.name}-${var.application.name}-gateway-${var.network.name}"
    network_mode             = "awsvpc"
    requires_compatibilities = ["FARGATE"]
    cpu                      = 256
    memory                   = 512
    execution_role_arn       = var.application.roles.task_execution.arn
    task_role_arn            = var.application.roles.task.arn
    container_definitions    = jsonencode([{
        name         = "${var.application.organization.name}-${var.application.name}-gateway-container"
        image        = "484737932465.dkr.ecr.us-east-1.amazonaws.com/${var.application.organization.name}-${var.application.name}-gateway:${var.branch}"
        essential    = true
        portMappings = [{
            protocol      = "tcp"
            containerPort = 80
            hostPort      = 80
        }, {
            protocol      = "tcp"
            containerPort = 443
            hostPort      = 443
        }]
        logConfiguration = {
            logDriver = "awslogs"
            options = {
                awslogs-group = "/ecs/${var.application.name}/${var.network.name}"
                awslogs-region = "us-east-1"
                awslogs-stream-prefix = "${var.application.name}_ECS"
            }
        }
    }])
    tags = {
        Name            = "${var.application.name} ${var.network.name} gateway task"
        ApplicationName = var.application.name
        BusinessOwner   = var.application.business_owner
        Division        = var.application.division.name
        Environment     = var.network.name
        OperationOwner  = var.application.operation_owner
    }
}

