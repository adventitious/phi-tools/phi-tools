resource "aws_ecs_cluster" "cluster" {
    name = "${var.application.organization.name}-${var.application.name}-cluster-${var.name}"
    tags = {
        Name            = "${var.application.name} ${var.name} ECS Cluster"
        ApplicationName = var.application.name
        BusinessOwner   = var.application.business_owner
        Division        = var.application.division.name
        Environment     = var.name
        OperationOwner  = var.application.operation_owner
    }
}

