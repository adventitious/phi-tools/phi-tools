variable "name" {
    description = "The name for the cluster"
    type = string
}

variable "application" {
    description = "The application object"
    type = object({
        name = string
        organization = object({
            name = string
        })
        division = object({
            name = string
        })
        business_owner = string
        operation_owner = string
    })
}

