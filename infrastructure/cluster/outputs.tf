output "name" {
    value = var.name
}

output "cluster" {
    value = aws_ecs_cluster.cluster
}
