
# image_repositories.tf
resource "aws_ecr_repository" "gateway_repo" {
    name                 = "${module.pcdu_application.organization.name}-${module.pcdu_application.name}-gateway"
    image_tag_mutability = "MUTABLE"
    tags = {
        Name            = "${module.pcdu_application.organization.name} ${module.pcdu_application.name} gateway repository"
        ApplicationName = module.pcdu_application.name
        BusinessOwner   = module.pcdu_application.business_owner
        Division        = module.pcdu_application.division.name
        Environment     = module.pcdu_application.name
        OperationOwner  = module.pcdu_application.operation_owner
    }
}

resource "aws_ecr_repository" "api_repo" {
    name                 = "${module.pcdu_application.organization.name}-${module.pcdu_application.name}-api"
    image_tag_mutability = "MUTABLE"
    tags = {
        Name            = "${module.pcdu_application.organization.name} ${module.pcdu_application.name} api repository"
        ApplicationName = module.pcdu_application.name
        BusinessOwner   = module.pcdu_application.business_owner
        Division        = module.pcdu_application.division.name
        Environment     = module.pcdu_application.name
        OperationOwner  = module.pcdu_application.operation_owner
    }
}


resource "aws_ecr_lifecycle_policy" "gateway_lifecycle_policy" {
    repository = aws_ecr_repository.gateway_repo.name
    policy = jsonencode({
        rules = [{
            rulePriority = 1
            description  = "keep last 10 images"
            action       = {
                type = "expire"
            }
            selection     = {
                tagStatus   = "any"
                countType   = "imageCountMoreThan"
                countNumber = 10
            }
        }]
    })
}

resource "aws_ecr_lifecycle_policy" "api_lifecycle_policy" {
    repository = aws_ecr_repository.api_repo.name
    policy = jsonencode({
        rules = [{
            rulePriority = 1
            description  = "keep last 10 images"
            action       = {
                type = "expire"
            }
            selection     = {
                tagStatus   = "any"
                countType   = "imageCountMoreThan"
                countNumber = 10
            }
        }]
    })
}
