resource "aws_s3_bucket" "static_bucket" {
    bucket = "${var.name}-static"
    acl = "private"
    versioning {
        enabled = true
    }

    policy = <<EOF
{
    "Id": "pcdu_static_bucket_policy",
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "bucket_policy_site_main",
            "Action": [
                "s3:GetObject"
            ],
            "Effect": "Allow",
            "Resource": "arn:aws:s3:::${var.bucket_name}/*",
            "Principal": "*"
        }
    ]
}
EOF

    tags {
        Name            = "${var.name} Static Assets"
        ApplicationName = var.name
        BusinessOwner   = var.business_owner
        Division        = var.division_name
        Environment     = var.name
        OperationOwner  = var.operation_owner
    }

    website {
        index_document = "index.html"
    }

}
