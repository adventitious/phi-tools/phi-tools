output "name" {
    value = var.name
}

output "organization" {
    value = {
        name = var.organization_name
        domain_name = var.organization_domain_name
    }
}

output "division" {
    value = {
        name = var.division
    }
}

output "business_owner" {
    value = var.business_owner
}

output "operation_owner" {
    value = var.operation_owner
}

output "roles" {
    value = {
        task = aws_iam_role.ecs_task_execution_role
        task_execution = aws_iam_role.ecs_task_execution_role
    }
}

output "website_domain" {
    value = "${aws_s3_bucket.static_bucket.website_domain}"
}

output "website_endpoint" {
    value = "${aws_s3_bucket.static_bucket.website_endpoint}"
}
