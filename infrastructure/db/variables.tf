variable "name" {
    description = "The name for the db"
    type = string
}

variable "availability_zones" {
    description = "The availability zones to enable for the network"
    type = object({
        names = list(string)
    })
}

variable "application" {
    description = "The application object"
    type = object({
        name = string
        organization = object({
            name = string
        })
        division = object({
            name = string
        })
        business_owner = string
        operation_owner = string
    })
}

variable "network" {
    description = "the dns_namespace"
    type = object({
        vpc = object({
            id = string
        })
        subnets = object({
            public = list(object({
                id = string
            }))
            private = list(object({
                id = string
            }))
        })
        dns_namespace = object({
            id = string
        })
    })
}

