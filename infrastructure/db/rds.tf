# Set up RDS

# ECS to RDS - Allows the PCDU API communicate to the RDS cluster.
resource "aws_security_group" "pcdu_rds" {
    name        = "PCDU-rds"
    description = "allow inbound access from the PCDU API tasks only"
    vpc_id      = var.network.vpc.id

    ingress {
        protocol    = "tcp"
        from_port   = "5432"
        to_port     = "5432"
        cidr_blocks = ["10.0.0.0/16"]
    }

    egress {
        protocol    = "-1"
        from_port   = 0
        to_port     = 0
        cidr_blocks = ["0.0.0.0/0"]
    }
}

resource "aws_db_subnet_group" "aurora_subnet_group" {
    name        = "dhhs-pcdu-aurora-db-subnet-group"
    description = "Allowed subnets for Aurora DB cluster instances"
    subnet_ids  = var.network.subnets.private.*.id
}

resource "aws_rds_cluster" "db" {
    cluster_identifier      = "dhhs-pcdu-dev-test-aurora-postgres-db"
    engine                  = "aurora-postgresql"
    availability_zones      = var.availability_zones.names
    db_subnet_group_name    = aws_db_subnet_group.aurora_subnet_group.name
    database_name           = "pcdu"
    master_username         = "pcdu"
    master_password         = "password"
    backup_retention_period = 5
    preferred_backup_window = "07:00-09:00"
    skip_final_snapshot     = true
}

resource "aws_rds_cluster_instance" "default" {
    cluster_identifier   = aws_rds_cluster.db.id
    db_subnet_group_name = aws_db_subnet_group.aurora_subnet_group.name
    count                = 1
    engine               = "aurora-postgresql"
    identifier           = "dhhs-pcdu-aurora-instance-${count.index + 1}"
    instance_class       = "db.t3.medium"

    lifecycle {
        create_before_destroy = true
    }
}

