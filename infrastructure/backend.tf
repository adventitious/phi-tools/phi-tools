# backend.tf
terraform {
    #backend "local" {
    #    path = "./terraform.tfstate"
    #}
    backend "s3" {
        bucket = "dhhs-pcdu-dev-test-terraform"
        key    = "terraform.tfstate"
        region = "us-east-1"
    }
}

