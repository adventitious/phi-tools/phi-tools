variable "name" {
    description = "The name of the environment"
    type = string
}

variable "branch" {
    description = "The branch to use for the environment"
    type = string
    default = "master"
}

variable "application" {
    description = "The application object"
    type = object({
        name = string
        organization = object({
            name = string
        })
        division = object({
            name = string
        })
        business_owner = string
        operation_owner = string
        roles = object({
            task = object({
                arn = string
            })
            task_execution = object({
                arn = string
            })
        })
    })
}

variable "network" {
    description = "the network in which to attach the container's network interface."
    type = object({
        name = string
        vpc = object({
            id = string
            arn = string
        })
        subnets = object({
            public = list(object({
                id = string
            }))
            private = list(object({
                id = string
            }))
        })
        dns_namespace = object({
            id = string
        })
        load_balancer = object({
            id = string
        })
    })
}

variable "cluster" {
    description = "The cluster in which to run the container"
    type = object({
        cluster = object({
            id = string
            name = string
        })
    })
}

variable "db" {
    description = "The db the app will use"
    type = object({
        endpoint = string
    })
}

variable "min_count" {
    description = "The minimum number of instances"
    type = number
    default = 1
}

variable "max_count" {
    description = "The maximum number of instances"
    type = number
    default = 2
}
