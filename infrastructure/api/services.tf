
# service.tf

# Two notable points here: This is the first time I had to use the lifecycle property
# of Terraform, which gives you control over how Terraform treats this resource during
# creation, update and destroying. In this case I had to tell Terraform that when I 
# run an update on my infrastructure, it should ignore which task definition is 
# currently set in the service and what is the desired number of task run by the service.

# The task definition I have to ignore because with every update/deployment of the 
# application, I create a new task definition revision, meaning the revision changes 
# outside of Terraform. So in order for Terraform not thinking it has to set the 
# task_definition back to a previous version, we have to ignore it.

# The desired number of tasks needs to be ignored because I also attached autoscaling
# rules to the service (more on that later) which allow the service to down- or upscale
# the number of tasks based on the load.

resource "aws_service_discovery_service" "api" {
    name = "api"
    dns_config {
        namespace_id = var.network.dns_namespace.id
        routing_policy = "MULTIVALUE"
        dns_records {
            ttl = 10
            type = "A"
        }
    }
    health_check_custom_config {
        failure_threshold = 5
    }
}
resource "aws_ecs_service" "api" {
    name                               = "dhhs-pcdu-api-preprod-service"
    cluster                            = var.cluster.cluster.id
    task_definition                    = aws_ecs_task_definition.api.arn
    desired_count                      = 2
    deployment_minimum_healthy_percent = 50
    deployment_maximum_percent         = 200
    launch_type                        = "FARGATE"
    scheduling_strategy                = "REPLICA"
    network_configuration {
        subnets          = var.network.subnets.public.*.id
        assign_public_ip = true
        security_groups  = [aws_security_group.ecs_tasks.id]
    }
    service_registries {
        registry_arn = aws_service_discovery_service.api.arn
    }
    #load_balancer {
    #    target_group_arn = aws_lb_target_group.pcdu_api_dev_test.arn
    #    container_name   = "dhhs-pcdu-api-container"
    #    container_port   = 4000
    #}
    lifecycle {
        ignore_changes = [task_definition, desired_count]
    }
    tags = {
        Name            = "PCDU Pre-Prod nginx ECS Service"
        ApplicationName = "PCDU"
        BusinessOwner   = "Medicaid Transformation"
        Division        = "DHB"
        Environment     = "Pre-Prod"
        OperationOwner  = "PCDU"
    }
}

