# autoscaling.tf

# As mentioned before, to make the setup super-sturdy, I also added some
# autoscaling rules to the service. The first thing I needed for this was
# an autoscaling target, defined like this:

resource "aws_appautoscaling_target" "ecs_target_api" {
    max_capacity       = 4
    min_capacity       = 1
    resource_id        = "service/${var.cluster.cluster.name}/${aws_ecs_service.api.name}"
    scalable_dimension = "ecs:service:DesiredCount"
    service_namespace  = "ecs"
}

# As you can see, I defined that the maximum number of task to be run should 
# be 4, while at least one task should be running at all time. And to this
# target, I can now attach rules.

# You can have multiple rules on when to scale the number of tasks, namely 
# based on either memory usage or cpu utilization. For demonstration purposes, 
# I added both rules:

resource "aws_appautoscaling_policy" "ecs_policy_memory_api" {
    name               = "memory-autoscaling"
    policy_type        = "TargetTrackingScaling"
    resource_id        = aws_appautoscaling_target.ecs_target_api.resource_id
    scalable_dimension = aws_appautoscaling_target.ecs_target_api.scalable_dimension
    service_namespace  = aws_appautoscaling_target.ecs_target_api.service_namespace
 
    target_tracking_scaling_policy_configuration {
        predefined_metric_specification {
            predefined_metric_type = "ECSServiceAverageMemoryUtilization"
        } 
        target_value       = 80
    }
}

resource "aws_appautoscaling_policy" "ecs_policy_cpu_api" {
    name               = "cpu-autoscaling"
    policy_type        = "TargetTrackingScaling"
    resource_id        = aws_appautoscaling_target.ecs_target_api.resource_id
    scalable_dimension = aws_appautoscaling_target.ecs_target_api.scalable_dimension
    service_namespace  = aws_appautoscaling_target.ecs_target_api.service_namespace 
    target_tracking_scaling_policy_configuration {
        predefined_metric_specification {
            predefined_metric_type = "ECSServiceAverageCPUUtilization"
        }
        target_value       = 60
    }
}

