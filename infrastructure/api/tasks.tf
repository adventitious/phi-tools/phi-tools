locals {
    api_environment = [{
        name: "DB_HOST",
        value: var.db.endpoint  
    }, {
        name: "DB_PORT",
        value: "5432"
    }, {
        name: "DB_USER",
        value: "pcdu",
    }, {
        name: "DB_PASSWORD",
        value: "password"
    }, {
        name: "DB_NAME",
        value: "pcdu"
    }]
    log_config =  {
        logDriver = "awslogs"
        options = {
            awslogs-group = "/ecs/${var.application.name}_${var.name}"
            awslogs-region = "us-east-1"
            awslogs-stream-prefix = "${var.application.name}_ECS"
        }
    }
}

resource "aws_ecs_task_definition" "api" {
    family                   = "${var.application.organization.name}-${var.application.name}-api-${var.branch}"
    network_mode             = "awsvpc"
    requires_compatibilities = ["FARGATE"]
    cpu                      = 256
    memory                   = 512
    execution_role_arn       = var.application.roles.task_execution.arn
    task_role_arn            = var.application.roles.task.arn
    container_definitions    = jsonencode([{
        name         = "${var.application.organization.name}-${var.application.name}-api"
        image        = "484737932465.dkr.ecr.us-east-1.amazonaws.com/${var.application.organization.name}-${var.application.name}-api:${var.branch}"
        essential    = true
        portMappings = [{
            protocol      = "tcp"
            containerPort = 4000
            hostPort      = 4000
        }]
        environment = local.api_environment
        logConfiguration = local.log_config
    }])
    tags = {
        Name            = "${var.application.name} api task"
        ApplicationName = var.application.name
        BusinessOwner   = var.application.business_owner
        Division        = var.application.division.name
        Environment     = var.name
        OperationOwner  = var.application.operation_owner
    }
}

resource "aws_ecs_task_definition" "api_ls_scripts" {
    family                   = "${var.application.organization.name}-${var.application.name}-api-ls-scripts-${var.branch}"
    network_mode             = "awsvpc"
    requires_compatibilities = ["FARGATE"]
    cpu                      = 256
    memory                   = 512
    execution_role_arn       = var.application.roles.task_execution.arn
    task_role_arn            = var.application.roles.task.arn
    container_definitions    = jsonencode([{
        name         = "${var.application.organization.name}-${var.application.name}-api-ls-scripts"
        image        = "484737932465.dkr.ecr.us-east-1.amazonaws.com/${var.application.organization.name}-${var.application.name}-api:${var.branch}"
        essential    = true
        portMappings = [{
            protocol      = "tcp"
            containerPort = 4000
            hostPort      = 4000
        }]
        command     = ["ls", "-la", "scripts"]
        environment = local.api_environment
        logConfiguration = local.log_config
    }])
    tags = {
        Name            = "${var.application.name} api ls scripts task"
        ApplicationName = var.application.name
        BusinessOwner   = var.application.business_owner
        Division        = var.application.division.name
        Environment     = var.name
        OperationOwner  = var.application.operation_owner
    }
}

resource "aws_ecs_task_definition" "api_ls" {
    family                   = "${var.application.organization.name}-${var.application.name}-api-ls-${var.branch}"
    network_mode             = "awsvpc"
    requires_compatibilities = ["FARGATE"]
    cpu                      = 256
    memory                   = 512
    execution_role_arn       = var.application.roles.task_execution.arn
    task_role_arn            = var.application.roles.task.arn
    container_definitions    = jsonencode([{
        name         = "${var.application.organization.name}-${var.application.name}-api-ls"
        image        = "484737932465.dkr.ecr.us-east-1.amazonaws.com/${var.application.organization.name}-${var.application.name}-api:${var.branch}"
        essential    = true
        portMappings = [{
            protocol      = "tcp"
            containerPort = 4000
            hostPort      = 4000
        }]
        command     = ["ls", "-la"]
        environment = local.api_environment
        logConfiguration = local.log_config
    }])
    tags = {
        Name            = "${var.application.name} api ls task"
        ApplicationName = var.application.name
        BusinessOwner   = var.application.business_owner
        Division        = var.application.division.name
        Environment     = var.name
        OperationOwner  = var.application.operation_owner
    }
}

resource "aws_ecs_task_definition" "api_drop_schema" {
    family                   = "${var.application.organization.name}-${var.application.name}-api-drop-schema-${var.branch}"
    network_mode             = "awsvpc"
    requires_compatibilities = ["FARGATE"]
    cpu                      = 256
    memory                   = 512
    execution_role_arn       = var.application.roles.task_execution.arn
    task_role_arn            = var.application.roles.task.arn
    container_definitions    = jsonencode([{
        name         = "${var.application.organization.name}-${var.application.name}-api-drop-schema"
        image        = "484737932465.dkr.ecr.us-east-1.amazonaws.com/${var.application.organization.name}-${var.application.name}-api:${var.branch}"
        essential    = true
        portMappings = [{
            protocol      = "tcp"
            containerPort = 4000
            hostPort      = 4000
        }]
        command     = ["sh", "scripts/drop.sh"]
        environment = local.api_environment
        logConfiguration = local.log_config
    }])
    tags = {
        Name            = "${var.application.name} api drop schema task"
        ApplicationName = var.application.name
        BusinessOwner   = var.application.business_owner
        Division        = var.application.division.name
        Environment     = var.name
        OperationOwner  = var.application.operation_owner
    }
}

resource "aws_ecs_task_definition" "api_rollback" {
    family                   = "${var.application.organization.name}-${var.application.name}-api-rollback-${var.branch}"
    network_mode             = "awsvpc"
    requires_compatibilities = ["FARGATE"]
    cpu                      = 256
    memory                   = 512
    execution_role_arn       = var.application.roles.task_execution.arn
    task_role_arn            = var.application.roles.task.arn
    container_definitions    = jsonencode([{
        name         = "${var.application.organization.name}-${var.application.name}-api-rollback"
        image        = "484737932465.dkr.ecr.us-east-1.amazonaws.com/${var.application.organization.name}-${var.application.name}-api:${var.branch}"
        essential    = true
        portMappings = [{
            protocol      = "tcp"
            containerPort = 4000
            hostPort      = 4000
        }]
        command     = ["npx", "knex", "migrate:rollback"]
        environment = local.api_environment
        logConfiguration = local.log_config
    }])
    tags = {
        Name            = "${var.application.name} api rollback task"
        ApplicationName = var.application.name
        BusinessOwner   = var.application.business_owner
        Division        = var.application.division.name
        Environment     = var.name
        OperationOwner  = var.application.operation_owner
    }
}
resource "aws_ecs_task_definition" "api_seed" {
    family                   = "${var.application.organization.name}-${var.application.name}-api-seed-${var.branch}"
    network_mode             = "awsvpc"
    requires_compatibilities = ["FARGATE"]
    cpu                      = 1024
    memory                   = 8192
    execution_role_arn       = var.application.roles.task_execution.arn
    task_role_arn            = var.application.roles.task.arn
    container_definitions    = jsonencode([{
        name         = "${var.application.organization.name}-${var.application.name}-api-seed"
        image        = "484737932465.dkr.ecr.us-east-1.amazonaws.com/${var.application.organization.name}-${var.application.name}-api:${var.branch}"
        essential    = true
        portMappings = [{
            protocol      = "tcp"
            containerPort = 4000
            hostPort      = 4000
        }]
        command     = ["npx", "knex", "seed:run"]
        environment = local.api_environment
        logConfiguration = local.log_config
    }])
    tags = {
        Name            = "${var.application.name} api seed task"
        ApplicationName = var.application.name
        BusinessOwner   = var.application.business_owner
        Division        = var.application.division.name
        Environment     = var.name
        OperationOwner  = var.application.operation_owner
    }
}
resource "aws_ecs_task_definition" "pcdu_api_migrate_preprod_dev" {
    family                   = "${var.application.organization.name}-${var.application.name}-api-migrate-${var.branch}"
    network_mode             = "awsvpc"
    requires_compatibilities = ["FARGATE"]
    cpu                      = 256
    memory                   = 512
    execution_role_arn       = var.application.roles.task_execution.arn
    task_role_arn            = var.application.roles.task.arn
    container_definitions    = jsonencode([{
        name         = "${var.application.organization.name}-${var.application.name}-api-migrate"
        image        = "484737932465.dkr.ecr.us-east-1.amazonaws.com/${var.application.organization.name}-${var.application.name}-api:${var.branch}"
        essential    = true
        portMappings = [{
            protocol      = "tcp"
            containerPort = 4000
            hostPort      = 4000
        }]
        command     = ["npx", "knex", "migrate:latest"]
        environment = local.api_environment
        logConfiguration = local.log_config
    }])
    tags = {
        Name            = "${var.application.name} api migrate task"
        ApplicationName = var.application.name
        BusinessOwner   = var.application.business_owner
        Division        = var.application.division.name
        Environment     = var.name
        OperationOwner  = var.application.operation_owner
    }
}

