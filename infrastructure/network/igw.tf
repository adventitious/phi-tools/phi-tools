resource "aws_internet_gateway" "igw" {
    vpc_id = aws_vpc.vpc.id
    tags = {
        Name            = "PCDU Pre-Prod Internet Gateway"
        ApplicationName = "PCDU"
        BusinessOwner   = "Medicaid Transformation"
        Division        = "DHB"
        Environment     = "Pre-Prod"
        OperationOwner  = "PCDU"
    }
}

