resource "aws_subnet" "public" {
    vpc_id                  = aws_vpc.vpc.id
    cidr_block              = cidrsubnet(aws_vpc.vpc.cidr_block, 8, count.index)
    availability_zone       = var.availability_zones.names[count.index]
    map_public_ip_on_launch = true
    count                   = length(var.availability_zones.names)
    tags = {
        Name            = "${var.application.name}-${var.name}-public-subnet-${var.availability_zones.names[count.index]}"
        ApplicationName = var.application.name
        BusinessOwner   = var.application.business_owner
        Division        = var.application.division.name
        Environment     = var.name
        OperationOwner  = var.application.operation_owner
    }
}

resource "aws_subnet" "private" {
    vpc_id            = aws_vpc.vpc.id
    cidr_block        = cidrsubnet(aws_vpc.vpc.cidr_block, 8, count.index + 5)
    availability_zone = var.availability_zones.names[count.index]
    count             = length(var.availability_zones.names)
    tags = {
        Name            = "${var.application.name}-${var.name}-private-subnet-${var.availability_zones.names[count.index]}"
        ApplicationName = var.application.name
        BusinessOwner   = var.application.business_owner
        Division        = var.application.division.name
        Environment     = var.name
        OperationOwner  = var.application.operation_owner
    }
}

