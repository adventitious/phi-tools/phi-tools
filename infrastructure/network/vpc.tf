# vpc.tf
# TODO change the name main to somthing better
resource "aws_vpc" "vpc" {
    cidr_block           = "10.0.0.0/16"
    enable_dns_support   = true
    enable_dns_hostnames = true
    # cidr_block = var.cidr
    tags = {
        Name            = "PCDU Pre-Prod VPC"
        ApplicationName = "PCDU"
        BusinessOwner   = "Medicaid Transformation"
        Division        = "DHB"
        Environment     = "Pre-Prod"
        OperationOwner  = "PCDU"
    }
}

