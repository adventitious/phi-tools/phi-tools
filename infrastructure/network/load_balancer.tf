resource "aws_lb" "lb" {
    name                             = "PCDU-lb-preprod"
    internal                         = false
    load_balancer_type               = "network"
    subnets                          = aws_subnet.public.*.id
    enable_deletion_protection       = false
    enable_cross_zone_load_balancing = "true"
    tags = {
        Name            = "PCDU Pre-Prod Load Balancer"
        ApplicationName = "PCDU"
        BusinessOwner   = "Medicaid Transformation"
        Division        = "DHB"
        Environment     = "Pre-Prod"
        OperationOwner  = "PCDU"
    }
}

# load_balancer.tf

# As you can see in the configuration above, it includes a reference to an
# ALB that will manage the distribution of requests to all the running tasks.

# The definition of the ALB is pretty straightforward, it consists of two
# listeners, one for HTTP and one for HTTPS, where the HTTP listener redirects
# to the HTTPS listener, which funnels traffic to the target group. This target
# group is later used by the ECS service to propagate the available tasks to.

# One thing for the ALB that I did not create with Terraform is the TLS
# certificate for HTTPS, the ARN for this I set as variable and passed it to
# the resource.
#resource "aws_acm_certificate" "https_certificate" {
#    domain_name       = "pcdu.dhhs.nc.gov"
#    validation_method = "DNS"
#
#    tags = {
#        Name = "PCDU DHHS Certificate"
#        Environment = "Pre-Prod"
#    }
#
#  lifecycle {
#    create_before_destroy = true
#  }
#}


#resource "aws_lb_target_group" "pcdu_api_dev_test" {
#    name        = "dhhs-pcdu-api-tg-dev-test"
#    port        = 4000
#    protocol    = "TCP"
#    vpc_id      = aws_vpc.pcdu_preprod.id
#    target_type = "ip"
#    health_check {
#        healthy_threshold   = "3"
#        unhealthy_threshold = "3"
#        interval            = "30"
#        protocol            = "TCP"
#    }
#    stickiness {
#        enabled = false
#        type = "lb_cookie"
#    }
#    tags = {
#        Name            = "PCDU Pre-Prod Load Balancer Dev/Test Target Group"
#        ApplicationName = "PCDU"
#        BusinessOwner   = "Medicaid Transformation"
#        Division        = "DHB"
#        Environment     = "Pre-Prod"
#        OperationOwner  = "PCDU"
#    }
#}


#resource "aws_lb_listener" "api" {
#    load_balancer_arn = aws_lb.pcdu_preprod.id
#    port              = 4000
#    protocol          = "TCP"
#    #ssl_policy        = "ELBSecurityPolicy-2016-08"
#    #certificate_arn = aws_acm_certificate.https_certificate.arn
#    #certificate_arn = "arn:aws:acm:us-east-1:484737932465:certificate/1b45f7a9-528e-4c80-91f6-91fe654d809a"
#    
#    default_action {
#        target_group_arn = aws_lb_target_group.pcdu_api_dev_test.id
#        type             = "forward"
#    }
#}

#resource "aws_lb_listener_certificate" "https_listener_certificate" {
#  listener_arn    = aws_alb_listener.https.arn
#  certificate_arn = aws_acm_certificate.https_certificate.arn
#}

