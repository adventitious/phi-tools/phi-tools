resource "aws_service_discovery_private_dns_namespace" "namespace" {
    name        = "${var.name}.${var.application.name}.${var.application.organization.name}.${var.application.organization.domain_name}.local"
    description = var.name
    vpc         = aws_vpc.vpc.id
}

