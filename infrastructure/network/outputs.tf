output "name" {
    value = var.name
}

output "vpc" {
    value = aws_vpc.vpc
} 

output "subnets" {
    value = {
        public = aws_subnet.public
        private = aws_subnet.private
    }
}

output "load_balancer" {
    value = aws_lb.lb
}

output "dns_namespace" {
    value = aws_service_discovery_private_dns_namespace.namespace
}

output "availability_zones" {
    value = var.availability_zones
}

output "discovery_dns" {
    value = "preprod.pcdu.dhhs.nc.gov"
}

