FROM --platform=linux/amd64 node:latest

ENV HUSKY_SKIP_INSTALL true
ENV CI true

WORKDIR /app
RUN chown node /app

USER node

RUN mkdir ./ssl

COPY package.json .
COPY package-lock.json .

RUN npm ci

ENV COOKIE_SECRET cookie_secret

COPY . .

CMD npm -v


