import {
    passport,
    LocalStrategy
} from "passport";

import { User } from "../lib/types/User.js";

passport.use(new LocalStrategy(async (username, password, done) => {
    try {
        const user = await User.findById({ id });
        if (user.verifyPassword(password)) {
            return done(user)
        }
    } catch (error) {
        console.log("login failure", error);
        return done(error);
    } 
});
