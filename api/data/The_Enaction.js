import { AgreementType } from "../lib/types/AgreementType.js";
import { PerformanceType } from "../lib/types/PerformanceType.js";
import { Obligation } from "../lib/types/Obligation.js";

export const agreement = new AgreementType({
    name: "The Invocation",
    data: {},
    content: `
# The Incantation


## Prelude

In order to celebrate, honor, explore, and promote inalienable interpretations and resolutions of truth, reality, morality, and discourse, apropos of nothing but this act itself, the invokers of this document establish and accept accountability to operate in accordance with the declarations and evidence described herein.


## Origination

Origination is an operation that declares the existence of an entity. Invokers of this document establish and accept the authority and responsibility to originate entities as needed to enable activity borne of this document.


### Entites

The existence of this act is evidence that the ability to originate an entity exists.


### Documents

The existence of this act is evidence that the ability to originate an invokable document that defines declarations and evidence.


## Agency

Agency is the quality imbued by an intrinsic ability of performance.  An entity with the ability to perform has agency to perform that action.


## Enactment

Enactment is a type of performance that operates on a document and confers accountability to operate in accordance with the doctrine and principles as defined in that document to the enactor.  


## Accountability

During the time within which an entity maintains enactment of a document, that entity is accountable by virtue of their enactment, and additionally through the mechanisms of the terms described within that document.


## Recantation

Any entity that enacts a document, may also recant their enactment. Recantation relinquishes that entity from the accountability to operate in accordance with the doctrine and principles defined in that document.
`
});

export const performanceTypes = [{
    name: "Origination"
}, {
    name: "Invocation"
}, {
    name: "Revocation"
}, {
    name: "Condemnation"
}, {
    name: "Censure"
}, {
    name: "Glorification"
}].map((performanceType) => {
    return new PerformanceType({
        ...performanceType
    });
});

export const obligations = performanceTypes.map((performanceType) => {
    return new Obligation({
        performance_type_id: performanceType.id,
        obligator_id: agreement.id
    });
});

export default [
    agreement,
    ...performanceTypes,
    ...obligations
];
