import { AgreementType } from "../lib/types/AgreementType.js";
import { PerformanceType } from "../lib/types/PerformanceType.js";
import { Obligation } from "../lib/types/Obligation.js";

export const agreement = new AgreementType({
    name: "Pledge to FightAnimal Cruelty",
    content: `
# The Pledge to Fight Animal Cruelty

Animals can’t speak for themselves, which is why it’s so important to use our voice to help them. Abuse, cruelty and neglect will never stop unless we all work together to make the world a better place for animals.
 
I PLEDGE TO:

* Stay alert for signs of animal cruelty.
* Report animal cruelty to law enforcement.
* Advocate for stronger laws to protect animals from cruelty.
* Sign up to receive important news and alerts from the ASPCA.
`
});

export const performanceTypes = [].map((performanceType) => {
    return new PerformanceType({
        ...performanceType
    });
});

export const obligations = performanceTypes.map((performanceType) => {
    return new Obligation({
        performance_type_id: performanceType.id,
        obligator_id: agreement.id
    });
});

export default [
    agreement,
    ...performanceTypes,
    ...obligations
];
