import { AgreementType } from "../lib/types/AgreementType.js";
import { PerformanceType } from "../lib/types/PerformanceType.js";
import { Obligation } from "../lib/types/Obligation.js";

export const agreement = new AgreementType({
    name: "Basic Needs",
    content: `
# Agreement to provide basic needs

Animals can’t speak for themselves, which is why it’s so important to use our voice to help them. Abuse, cruelty and neglect will never stop unless we all work together to make the world a better place for animals.
 
I PLEDGE TO:

* Stay alert for signs of animal cruelty.
* Report animal cruelty to law enforcement.
* Advocate for stronger laws to protect animals from cruelty.
* Sign up to receive important news and alerts from the ASPCA.
`
});

export const performanceTypes = [{
    name: "Provide Bread",
    content: "Will provide a 1kg loaf of bread to party in need"
}, {
    name: "Provide Rice",
    content: "Will provide a 1kg of white or brown rice to party in need"
}, {
    name: "Provide Water",
    content: "Will provide a 10L of clean drinking water to party in need"
}, {
    name: "Provide Shelter",
    content: "Will provide shelter to party in need"
}, {
    name: "Provide Heat",
    content: "Will set up and configure a heating system for a party in need"
}, {
    name: "Provide Beans",
    content: "Will provide a 1kg of beans or lentils to party in need"
}].map((performanceType) => {
    return new PerformanceType({
        ...performanceType
    });
});

export const obligations = performanceTypes.map((performanceType) => {
    return new Obligation({
        performance_type_id: performanceType.id,
        obligator_id: agreement.id
    });
});

export default [
    agreement,
    ...performanceTypes,
    ...obligations
];
