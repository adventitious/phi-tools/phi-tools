
export const maybeOffset = (offset) => (q) => {
    if (Number(offset) >= 0) {
        return q.offset(offset);
    }
    return q;
};

export const maybeLimit = (limit) => (q) => {
    if (Number(limit) >= 0) {
        return q.limit(limit);
    }
    return q;
};

