export const tableNames = [
    "documents",
    "entities",
    "nodes",
    "obligations",
    "performances",
    "performance_types"
];

export const validateUuid = (toCheck) => {
    if (typeof toCheck !== "string") {
        return false;
    }
    const subpartLengths = toCheck.split("-").map((subpart) => {
        return Number(`0x${subpart}0`) ? subpart.length : "";
    });

    return subpartLengths.toString() === "8,4,4,4,12";
};

