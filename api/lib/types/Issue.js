import { v4 } from "uuid";

import { Node } from "../Node/index.js";

export const Issue = ({ id, name }) => {
    if (typeof id === "undefined") {
        id = v4();
    }
    return {
        ...Node({ id }),
        issue: { id, name }
    };
};

export const typeDefs = `
    type Issue {
        name: String
    }

    type Query {
        issue(id: ID!): Issue
        issues: [Issue]
    }

    type Mutation {
        addIssue(name: String!): Issue
    }
`;

export const resolvers = {};
