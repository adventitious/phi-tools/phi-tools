import { v4 } from "uuid";

export const Permission = ({ id, specifier }) => {
    if (typeof id === "undefined") {
        id = v4();
    }
    return {
        permission: {
            id,
            specifier
        }
    };
};

export const typeDefs = `
    type Permission {
        specifier: String
    }

    type Query {
        permission: Permission
        permissions: [Permission]
    }

    type Mutation {
        addPermission(specifier: String!): Permission
        removePermission(id: ID!): Boolean
    }
`;
export const resolvers = {};

