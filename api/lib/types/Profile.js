import { maybeOffset, maybeLimit } from "../db/helpers.js";
import { insertData } from "../db/insertData.js";
import { knex } from "../db/knex.js";
import { pipe } from "../utils/pipe.js";

import { Document } from "./Document.js";

export class Profile extends Document {
    static async findById(id) {
        if (id === "new") return null;
        const data = await knex("profiles").where({ id });
        return new Document(data[0]);
    }
    static async all({ offset, limit }) {
        const data = await pipe(knex("profiles"))([
            maybeOffset(offset),
            maybeLimit(limit)
        ]);
        return data.map((row) => new Document(row));
    }
    constructor(data) {
        super(data);
        if (typeof data.uri === "undefined") {
            data.uri === "";
        }
        this._name = data.name;
        this._uri = data.uri;
        this._content = data.content;
    }
    async save() {
        return await insertData([this]);
    }
    get name() {
        return this._name;
    }
    get uri() {
        return this._uri;
    }
    get content() {
        return this._content;
    }
    get normalized() {
        return {
            ...super.normalized,
            profile: {
                id: this.id,
                name: this.name,
                uri: this.uri,
                content: this.content,
            }
        };
    }
}

export const typeDefs = `
    input ProfileInput {
        name: String
        documentTypeId: ID
        content: String
    }

    type Profile {
        id: ID
        name: String
        kind: String
        content: JSON
    }

    type Query {
        profile(id: ID!): Profile
        profiles: [Profile]
    }

    type Mutation {
        createProfile(profile: ProfileInput)
        updateProfile(id: ID, profile: ProfileInput): Profile
        removeProfile(id: ID!): Boolean
    }
`;
    
export const resolvers = {
    Query: {
        profile: (root, { id }) => Profile.findById(id),
        profiles: (root, { limit, offset }) => Profile.all({ limit, offset })
    },
    Mutation: {
        createProfile: (root, { profile }) => (new Profile(profile))
            .save(),
        updateProfile: (root, { id, profile }) => Profile.findById(id)
            .assign(profile)
            .save()
    }
};
