import { knex } from "../db/knex.js";
import { insertData } from "../db/insertData.js";
import { maybeLimit, maybeOffset } from "../db/helpers.js";
import { pipe } from "../utils/pipe.js";

import { Document } from "./Document.js";

export class AgreementType extends Document {
    static async all({ limit, offset }) {
        const data = await pipe(knex("agreement_types as at")
            .select("d.*", "at.*")
            .innerJoin("documents as d", "d.id", "=", "at.id")
        )([
            maybeLimit(limit),
            maybeOffset(offset)
        ]);
        console.log(data);
        return data.map((row) => new this(row));
    }
    static async findById(id) {
        if (id === "new") {
            return null;
        }
        const data = await knex("agreement_types as at")
            .select("d.*", "at.*")
            .where({ "at.id": id })
            .innerJoin("documents as d", "d.id", "=", "at.id");
        console.log(data);
        if (typeof data[0] === "undefined") {
            return null;
        }
        return new this(data[0]);
    }
    static async delById(id) {
        await knex("agreement_types")
            .where({ id })
            .del();
    }
    constructor(data) {
        super(data);
    }
    assign(data) {
        Object.assign(this, data);
    }
    async save() {
        await insertData([this]);
    }
    get normalized() {
        return {
            ...super.normalized,
            agreement_type: { id: this.id },
        };
    }
}

export const typeDefs = `
    input AgreementTypeInput {
        name: String
        content: String
    }
    type AgreementType {
        id: ID!
        name: String
        content: String
    }
    type Query {
        agreementType(id: ID!): AgreementType
        agreementTypes: [AgreementType]
    }
    type Mutation {
        createAgreementType(agreementType: AgreementTypeInput): AgreementType
        updateAgreementType(id: ID!, agreementType: AgreementTypeInput): AgreementType
        deleteAgreementType(id: ID!): Boolean
    }
`;
export const resolvers = {
    Query: {
        agreementType: (root, { id }) => AgreementType
            .findById(id),
        agreementTypes: (root,  { limit, offset }) => AgreementType
            .all({ limit, offset })
    },
    Mutation: {
        createAgreementType: (root, { agreementType }) => (new AgreementType(agreementType))
            .save(),
        updateAgreementType: (root, { id, agreementType }) => AgreementType
            .findById(id)
            .set(agreementType)
            .save(),
        deleteAgreementType: (root, { id }) => AgreementType
            .delById(id)
    }
};

