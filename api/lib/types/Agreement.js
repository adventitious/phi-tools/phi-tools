import { v4 } from "uuid";

import { knex } from "../db/knex.js";
import { maybeLimit, maybeOffset } from "../db/helpers.js";
import { pipe } from "../utils/pipe.js";

import { AgreementType } from "./AgreementType.js";
import { Document } from "./Document.js";


export class Agreement extends Document {
    static async findAll({ limit, offset }) {
        const q = pipe(knex("agreements as a")
            .select("d.*", "a.*")
            .innerJoin("documents as d", "d.id", "=", "a.id")
        )([
            maybeLimit(limit),
            maybeOffset(offset)
        ]);
        return (await q).map((row) => new Agreement(row));
    }
    static async findById(id) {
        if (id === "new") {
            return new Agreement({});
        }
        return (await knex("agreements as a")
            .select("d.*", "a.*")
            .where({ "a.id": id })
            .innerJoin("documents as d", "d.id", "=", "a.id")
        )[0];
    }
    static async create({
        name,
        content,
        data,
        agreementType
    }) {
        return new this({
            name,
            content,
            data,
            agreementType
        });
    }
    constructor({
        id,
        name,
        content,
        data,
        agreementType
    }) {
        if (typeof id === "undefined") {
            this.id = v4();
        }
        this.document = new Document({
            id,
            name,
            content,
            data
        });
        this.agreementType = agreementType;
    }
    normalized() {
        return {
            agreement: {
                id: this.id,
                agreementTypeId: this.agreementType.id
            },
            ...this.document.normalized()
        };
    }
    save() {
        
    }
}

export const typeDefs = `
    type Agreement {
        id: ID!
        name: String
        content: String
        agreementType: AgreementType
    }

    type Query {
        agreement(id: ID!): Agreement
        agreements: [Agreement]
    }

    type Mutation {
        createAgreement(name: String!): Agreement
    }
`;

export const resolvers = {
    Agreement: {
        agreementType: ({ agreementTypeId }) => AgreementType.findById(agreementTypeId)
    },
    Query: {
        agreement: (root, { id }) => Agreement.findById(id),
        agreements: (root, { limit, offset }) => Agreement.findAll({ limit, offset })
    },
    Mutation: {
        createAgreement: (root, { name, title, memo }) => Agreement.create({
            name,
            title,
            content: memo
        })
    }
};

