import { knex } from "../db/knex.js";

import { Node } from "./Node.js";

export const Obligation = class extends Node {
    constructor(data) {
        super(data);
        this._obligatorId = data.obligatorId;
        this._agreementTypeId = data.agreementTypeId;
    }
    get obligatorId() {
        return this._obligatorId;
    }
    get agreementTypeId() {
        return this._agreementTypeId;
    }
    get normalized() {
        return {
            ...super.normalized,
            obligation: {
                id: this.id,
                obligator_id: this.obligatorId,
                agreement_type_id: this.agreementTypeId
            }
        };
    }
};

export const typeDefs = `
    type Obligation {
        name: String
    }

    type Query {
        obligation: Obligation
        obligations: [Obligation]
    }

    type Mutation {
        addObligation(name: String!): Obligation
        removeObligation(id: ID!): Boolean
    }
`;

export const resolvers = {
    Obligation: {},
    Query: {
        obligation: async (root, { id }) => {
            return (await knex("obligations").where({ id }))[0];
        },
        obligations: () => {
            return knex("obligations");
        }
    }
};
