import { v4 } from "uuid";

export const Evidence = ({ id, document_id }) => {
    if (typeof id === "undefined") {
        id = v4();
    }
    return {
        evidence: {
            id,
            document_id,
        }
    };
};

export const typeDefs = `
    type Evidence {
        name: String
    }

    type Query {
        evidence: [Evidence]
    }

    type Mutation {
        addEvidence(name: String!): Evidence
    }
`;

export const resolvers = {};

