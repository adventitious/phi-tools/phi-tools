import {
    v4,
    validate as validateUuid
} from "uuid";

import { sleep } from "../utils/sleep.js";

import { Extensible } from "./Extensible.js";

export const Node = class extends Extensible {
    constructor(data) {
        super();
        if (typeof data.id === "undefined") {
            this._minted = false;
        }
        if (validateUuid(data.id)) {
            this._id = data.id;
        }
    }
    get id() {
        return this._id;
    }
    get normalized() {
        return {
            node: {
                id: this.id
            }
        };
    }
    async mint(ensureUnique=async () => await true, retries=5, delay=200) {
        if (this._minted) {
            throw "DOUBLE MINT";
        }
        const id = v4();
        console.log("Unique?", await ensureUnique(id));
        console.log(this);
        if (await ensureUnique(id)) {
            this._id = id;
            this._minted = true;
            return this;
        }
        if (retries > 1) {
            await sleep(delay);
            return await this.mint(ensureUnique, retries - 1);
        }
    }
};

export const typeDefs = `
    type Node {
        id: ID
    }

    type Query {
        node(id: ID): Node
        nodes: [Node]
    }

    type Mutation {
        addNode(name: String!): Node
    }
`;

export const resolvers = {};
