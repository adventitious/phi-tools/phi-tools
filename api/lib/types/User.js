import { v4 } from "uuid";

import fetch from "node-fetch";
import { tableize } from "inflected";


import { maybeOffset, maybeLimit } from "../db/helpers.js";
import { insertData } from "../db/insertData.js";
import { knex } from "../db/knex.js";
import { matrixClient } from "../matrix.js";
import { pipe } from "../utils/pipe.js";

import { Entity } from "./Entity.js";


export class User extends Entity {

    constructor(data) {
        super(data);
    }

    get normalized() {
        if (typeof id === "undefined") {
            id = v4();
        }
        return {
            ...Entity({ id }),
            user: { id, name }
        };
    }
};

export const typeDefs = `
    input UserInput {
        userId: ID
        username: String
        name: String
    }

    input UserRegistrationInput {
        userId: ID
        username: String
        password: String
        name: String
    }

    type User {
        name: String
        id: ID
    }

    type Query {
        user: User
        users: [User]
    }

    type Mutation {
        addUser(name: String!): User
        register(user: UserRegistrationInput): User
    }
`;

export const resolvers = {
    Query: {
        user: (root, { id }) => User.findById({id}),
        users: (root, { limit, offset }) => User.all({ limit, offset })
    },
    Mutation: {
        addUser: (root, { id, user }) => User
            .findById({ id })
            .assign(user)
            .save(),
        register: async (root, { user }) => {
            await matrixClient.register(
                user.username,
                user.password,
                null,
                { type: "m.login.dummy" }
            )
        }
    }
};
