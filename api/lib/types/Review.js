import { v4 } from "uuid";

import { Node } from "./Node.js";

export const Review = ({ id, evidence_id, reviewer_id }) => {
    if (typeof id === "undefined") {
        id = v4();
    }
    return {
        ...Node({ id }),
        review: {
            id,
            evidence_id,
            reviewer_id
        }
    };
};

export const typeDefs = `
    type Review {
        id: ID!
        evidenceId: ID!
        reviewerId: ID!
    }

    type Query {
        review: Review
        reviews: [Review]
    }

    type Mutation {
        addReview(
            evidenceId: ID!,
            reviewerId: ID!
        ): Review
    }
`;

export const resolvers = {};
