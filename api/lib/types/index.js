import { mergeTypeDefs } from "@graphql-tools/merge";
import { makeExecutableSchema } from "@graphql-tools/schema";

import {
    typeDefs as scalarTypeDefs,
    resolvers as scalarResolvers
} from "graphql-scalars";

import {
    typeDefs as agreementTypeDefs,
    resolvers as agreementResolvers
} from "./Agreement.js";
import {
    typeDefs as agreementTypeTypeDefs,
    resolvers as agreementTypeResolvers
} from "./AgreementType.js";
import {
    typeDefs as agreementRoleTypeDefs,
    resolvers as agreementRoleResolvers
} from "./AgreementRole.js";
import {
    typeDefs as assignmentTypeDefs,
    resolvers as assignmentResolvers
} from "./Assignment.js";
import {
    typeDefs as documentTypeDefs,
    resolvers as documentResolvers
} from "./Document.js";
import {
    typeDefs as entityTypeDefs,
    resolvers as entityResolvers
} from "./Entity.js";
import {
    typeDefs as evidenceTypeDefs,
    resolvers as evidenceResolvers
} from "./Evidence.js";
import {
    typeDefs as obligationTypeDefs,
    resolvers as obligationResolvers
} from "./Obligation.js";
import {
    typeDefs as organizationTypeDefs,
    resolvers as organizationResolvers
} from "./Organization.js";
import {
    typeDefs as performanceTypeDefs,
    resolvers as performanceResolvers
} from "./Performance.js";
import {
    typeDefs as performanceTypeTypeDefs,
    resolvers as performanceTypeResolvers
} from "./PerformanceType.js";
import {
    typeDefs as permissionTypeDefs,
    resolvers as permissionResolvers
} from "./Permission.js";
import {
    typeDefs as reactionTypeDefs,
    resolvers as reactionResolvers
} from "./Reaction.js";
import {
    typeDefs as reviewTypeDefs,
    resolvers as reviewResolvers
} from "./Review.js";
import {
    typeDefs as roleTypeDefs,
    resolvers as roleResolvers
} from "./Role.js";
import {
    typeDefs as userTypeDefs,
    resolvers as userResolvers
} from "./User.js";

export const typeDefs = mergeTypeDefs([
    scalarTypeDefs,
    agreementTypeTypeDefs,
    agreementTypeDefs,
    agreementRoleTypeDefs,
    assignmentTypeDefs,
    documentTypeDefs,
    entityTypeDefs,
    evidenceTypeDefs,
    obligationTypeDefs,
    organizationTypeDefs,
    performanceTypeDefs,
    performanceTypeTypeDefs,
    permissionTypeDefs,
    reactionTypeDefs,
    reviewTypeDefs,
    roleTypeDefs,
    userTypeDefs
]);

export const resolvers = [
    scalarResolvers,
    agreementTypeResolvers,
    agreementResolvers,
    agreementRoleResolvers,
    assignmentResolvers,
    documentResolvers,
    entityResolvers,
    evidenceResolvers,
    obligationResolvers,
    organizationResolvers,
    performanceResolvers,
    performanceTypeResolvers,
    permissionResolvers,
    reactionResolvers,
    reviewResolvers,
    roleResolvers,
    userResolvers
].reduce((partialResolvers, resolversForType) => {
    return Object.entries(resolversForType).reduce((partialResolvers, [key, value]) => {
        return {
            ...partialResolvers,
            [key]: {
                ...partialResolvers[key],
                ...value
            }
        };
    }, partialResolvers);
}, {});

export const schema = makeExecutableSchema({
    typeDefs,
    resolvers
});

