import { v4 } from "uuid";

export const Assignment = ({ id, assignee_id, node_id }) => {
    if (typeof id === "undefined") {
        id = v4();
    }
    return {
        assignment: {
            id,
            assignee_id,
            node_id
        }
    };
};

export const typeDefs = `
    type Assignment {
        name: String
    }

    type Query {
        assignment: Assignment
        assignments: [Assignment]
    }

    type Mutation {
        addAssignment(name: String!): Assignment
    }
`;

export const resolvers = {};
