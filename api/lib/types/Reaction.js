import { v4 } from "uuid";

import { Node } from "./Node.js";

export const Reaction = ({ id, issue_id, user_id }) => {
    if (typeof id === "undefined") {
        id = v4();
    }
    return {
        ...Node({ id }),
        review: {
            id,
            issue_id,
            user_id
        }
    };
};

export const typeDefs = `
    type Reaction {
        kind: String
    }

    type Query {
        reaction: Reaction
        reactions: [Reaction]
    }

    type Mutation {
        addReaction(name: String!): Reaction
    }
`;

export const resolvers = {};
