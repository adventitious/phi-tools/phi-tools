import { intersection } from "./intersection.js";

export const coalesce = (xs, ys) => intersection(Object.keys(xs), Object.keys(ys))
    .map(key => [xs[key], ys[key]]);
