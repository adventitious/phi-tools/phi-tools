export const pipe = (initial) => (functions) => functions.reduce((a, b) => b(a), initial);
