export const dissect = (objects) => {
    return objects.reduce((partial, object) => {
        return Object.entries(object).reduce((partial, [key, value]) => {
            return {
                ...partial,
                [key]: [...value, ...partial[key]]
            };
        }, partial);
    }, {});
};

